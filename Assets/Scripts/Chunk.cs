﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chunk : MonoBehaviour
{
    public ChunkMngr chunkMngr; // connector to the chunk

    public MeshRenderer meshRenderer; // renderers and mngrs for the mesh
    public MeshFilter meshFilter;
    public MeshCollider meshCollider;

    public int MyChunkHeight; // the chunk height and width
    public int MyChunkWidth;

    int vertexIndex = 0;
    List<Vector3> vertices = new List<Vector3>(); // data for creating the object
    List<int> triangles = new List<int>();
    List<Vector2> uvs = new List<Vector2>();
    List<Vector3> positions = new List<Vector3>();

    byte[,,] voxelMap = new byte[VoxelData.ChunkWidth, VoxelData.ChunkMaxHeight, VoxelData.ChunkWidth]; // the blocks in the object
    Item[,,] voxelMapReferences = new Item[VoxelData.ChunkWidth, VoxelData.ChunkMaxHeight, VoxelData.ChunkWidth]; // the interactive blocks in the object

    public bool Populated = false; // marks that the populating is done

    public bool getHeight;

    public bool Last = false;
    void Start()
    {
        Populated = false;
        chunkMngr = GameObject.FindGameObjectWithTag("ChunkMngr").GetComponent<ChunkMngr>(); // get the chunkMngr
        MyChunkHeight = VoxelData.ChunkHeight;
        MyChunkWidth = VoxelData.ChunkWidth;
        //print(MyChunkWidth);
        PopulateVoxelMap(); // create original chunk
        Populated = true;
        if (Last)
        {
            chunkMngr.CallTreeGen();
        }
        CreateMeshData();
        CreateMesh();
        // print("chunk created");
        meshRenderer.enabled = false;
    }

    /*void Update()
    {
    }*/

    void PopulateVoxelMap()
    {
        for (int z = 0; z < MyChunkWidth; z++) // run on the width
        {
            for (int x = 0; x < MyChunkWidth; x++) // run on the width
            {

                int maxY = (int)(Mathf.PerlinNoise(((x + transform.position.x) / 1.5f + VoxelData.seed) / VoxelData.terrainDetail, ((z + transform.position.z) / 1.5f + VoxelData.seed) / VoxelData.terrainDetail) * VoxelData.terrainHeight); // get perline at world pos
                maxY += VoxelData.groundHeight; // add ground height

                if (maxY > VoxelData.ChunkMaxHeight) { maxY = VoxelData.ChunkMaxHeight; } // if aout of chunk height, set to maxHeight 

                MyChunkHeight = (maxY > MyChunkHeight) ? maxY : MyChunkHeight; // if maxY grater than myChunkHeight, myChunkHeight = maxY

                int bedrock_height1 = Random.Range(0, 3); // bedrock levels
                int bedrock_height2 = Random.Range(0, 3); // bedrock levels

                for (int y = 0; y < maxY; y++) // run on the maxY
                {

                    if (y == maxY - 1) // generate grass at top
                    {
                        if (y > VoxelData.StoneHeight)
                        {
                            voxelMap[x, y, z] = 3;
                        }
                        else if (y > VoxelData.SnowHeight)
                        {
                            voxelMap[x, y, z] = 10;
                        }
                        else
                        {
                            voxelMap[x, y, z] = 1;
                        }
                    }
                    else if ((maxY - y) - Random.Range(1, 3) < 3) // genereate dirt at a 1-3 layer below the grass
                    {
                        if (y <= VoxelData.StoneHeight)
                        {
                            voxelMap[x, y, z] = 2;
                        }
                        else
                        {
                            voxelMap[x, y, z] = 3;
                        }
                    }
                    else if (y != bedrock_height1 && y != bedrock_height2 && y != 0) // stone if not in bedrock pos
                    {
                        int ran = Random.Range(1, 15); // random between stone and cobblestone
                        if (ran == 1) // cobblestone
                        {
                            voxelMap[x, y, z] = 6;
                        }
                        else // stone
                        {
                            voxelMap[x, y, z] = 3;
                        }
                    }
                    else if (y == 0 || y == bedrock_height2 || y == bedrock_height1) // if in bottom of world or in bedrock_height
                    {
                        voxelMap[x, y, z] = 4;
                    }
                }
            }
        }

    } // O(25*maxY) // generate the original map
    void CreateMeshData()
    {
        for (int y = 0; y < VoxelData.ChunkMaxHeight; y++)
        {
            for (int x = 0; x < VoxelData.ChunkWidth; x++)
            {
                for (int z = 0; z < VoxelData.ChunkWidth; z++)
                {
                    if (voxelMap[x, y, z] != 0) // if block in position, add vector to chunk
                    {
                        AddVoxelDataToChunk(new Vector3(x, y, z));
                    }
                }
            }
        }

    } // O(25*height*6) -> O(150*height) // create the mesh data of the map
    Item CheckVoxel(Vector3 pos)
    {

        int x = Mathf.FloorToInt(pos.x); // get normalized pos
        int y = Mathf.FloorToInt(pos.y);
        int z = Mathf.FloorToInt(pos.z);

        if ((x < 0 || x > MyChunkWidth - 1 || y < 0 || y > MyChunkHeight - 1 || z < 0 || z >= MyChunkWidth)) // out of bounds
            return null;

        try
        {
            return BlockData.ItemTypesArr[voxelMap[x, y, z]]; // return block
        }
        catch (System.Exception e)
        {
            print("Error: " + e.Message + ";;; index: " + voxelMap[x, y, z]);
            return null;
        }
    } // O(1) // get the block at pos (null if out of bounds)
    void AddVoxelDataToChunk(Vector3 pos)
    {
        int index = voxelMap[(int)pos.x, (int)pos.y, (int)pos.z];
        //print(BlockData.BlockTypesArr[index].type.ToString() + ", " + index + ", " + BlockData.BlockTypesArr[index].worldPosition);
        for (int p = 0; p < 6; p++)
        {  // run on faces

            if (BlockData.ItemTypesArr[index].type != BlockData.BlockTypes.Air && (CheckVoxel(pos + VoxelData.faceChecks[p]) == null || CheckVoxel(pos + VoxelData.faceChecks[p]).transparent))
            { // if next to empty or to transparent

                vertices.Add(pos + VoxelData.voxelVerts[VoxelData.voxelTris[p, 0]]); // add data to chunk data
                vertices.Add(pos + VoxelData.voxelVerts[VoxelData.voxelTris[p, 1]]);
                vertices.Add(pos + VoxelData.voxelVerts[VoxelData.voxelTris[p, 2]]);
                vertices.Add(pos + VoxelData.voxelVerts[VoxelData.voxelTris[p, 3]]);
                uvs.Add(BlockData.voxelUvs[index - 1, p, 0]);
                uvs.Add(BlockData.voxelUvs[index - 1, p, 1]);
                uvs.Add(BlockData.voxelUvs[index - 1, p, 2]);
                uvs.Add(BlockData.voxelUvs[index - 1, p, 3]);
                triangles.Add(vertexIndex);
                triangles.Add(vertexIndex + 1);
                triangles.Add(vertexIndex + 2);
                triangles.Add(vertexIndex + 2);
                triangles.Add(vertexIndex + 1);
                triangles.Add(vertexIndex + 3);

                positions.Add(pos);

                vertexIndex += 4;
            }
        }

    } // O(6) // add voxel data to lists
    void CreateMesh()
    {

        Mesh mesh = new Mesh
        {
            vertices = vertices.ToArray(),
            triangles = triangles.ToArray(),
            uv = uvs.ToArray()
        }; // create a new mesh

        mesh.RecalculateNormals();

        meshFilter.mesh = mesh;
        meshCollider.sharedMesh = mesh;
    } // O(1) // create the mesh and texture

    /*Item CheckVoxelNextChunk(Vector3 BlockPos)
    {
        Chunk nxtChunk = chunkMngr.GetChunkAtPos(BlockPos);

        if (nxtChunk && nxtChunk.Populated)
        {
            Vector3 ret = BlockPos - nxtChunk.gameObject.transform.position;
            ret.x = (int)(ret.x);
            ret.y = (int)(ret.y);
            ret.z = (int)(ret.z);
            return nxtChunk.CheckVoxel(ret);
        }
        else
        {
            return null;
        }
    }
    */

    public void HitByPlayer(Vector3 position, Vector3 surfaceNormal)
    {
        Vector3 normal = NormalizeHit(position, surfaceNormal);
        Item ret_itm = null;
        //float start = Time.time;
        if (BlockData.ItemTypesArr[voxelMap[(int)normal.x, (int)normal.y, (int)normal.z]].type != BlockData.BlockTypes.Bedrock || Data.GameMode_int == 1)
        {
            ret_itm = voxelMapReferences[(int)normal.x, (int)normal.y, (int)normal.z];
            if (ret_itm == null)
            {
                ret_itm = BlockData.ItemTypesArr[voxelMap[(int)normal.x, (int)normal.y, (int)normal.z]];
            }
            voxelMapReferences[(int)normal.x, (int)normal.y, (int)normal.z] = null;
            voxelMap[(int)normal.x, (int)normal.y, (int)normal.z] = 0;


            UpdateMesh();

            Vector3 ret = position;
            ret.x = ((float)(int)(ret.x) - ((surfaceNormal.x > 0) ? 1 : 0));
            ret.y = ((float)(int)(ret.y) - ((surfaceNormal.y > 0) ? 1 : 0));
            ret.z = ((float)(int)(ret.z) - ((surfaceNormal.z > 0) ? 1 : 0));

            //print(ret + (Vector3.one / 2));

            ret_itm.SetLight(false, ret); // close light source
            ret_itm.OnDestroy();

            if (Data.GameMode_int == 1)
            {
                chunkMngr.MainScript.inventory.addBlock(ret_itm);
            }
            else
            {
                if (ret_itm.type != BlockData.BlockTypes.Stone)
                {
                    chunkMngr.MainScript.inventory.DropItemFunc(ret + (Vector3.one / 2), ret_itm, 0);
                }
                else
                {
                    chunkMngr.MainScript.inventory.DropItemFunc(ret + (Vector3.one / 2), new Block_Cobblestone(ret_itm.ParentChunk, ret_itm.worldPosition), 0);
                }
            }
            if (Data.GameMode_int != 1)
            {
                chunkMngr.MainScript.inventory.UpdateDurabilityOfHeldTool();
            }
        }
        //float end = Time.time;
        //print("O(150*height): " + 150*MyChunkHeight + "time: " + (end-start));
    } // called when aa player hits a block. position is the world position of the hit, surfaceNormal is the direction it was hit from // O(150*height)

    /* public void Remove_Item(Vector3 pos)
    {
        int index = positions.IndexOf(pos);

        int count = 0;
        while (positions.Contains(pos))
        {
            positions.Remove(pos);
            count++;
        }

        vertices.RemoveRange(index * 4, 4 * count);
        uvs.RemoveRange(index * 4, 4 * count);
        triangles.RemoveRange(index * 6, 6 * count);

        int[] arr = triangles.ToArray();
        int triangleValue = arr[(index * 6)];
        bool action = false;
        int i = 0;
        foreach (int item in arr)
        {
            action = (action) ? action : (item == triangleValue);
            if (action)
            {
                triangles.RemoveAt(i);
                triangles.Insert(i, item - 4);
                //print("item: " + item + ", -4: " + (item - 4));
            }
            i++;
            if (i == triangles.Count)
            {
                print("LastItem: " + item);
            }
        }
        print("triangleValue: " + triangleValue);

        triangles.ForEach(disp);

    }
    void disp(int val)
    {
        print("item: " + val);
    }*/

    public float GetDistance(Vector3 playerPos) // get distance from my position to given position (returns -1 if in the object)
    {
        //print("getDistance: " + playerPos+", "+transform.position + ", " + (transform.position + new Vector3(MyChunkWidth,0,MyChunkWidth)) + ", " + MyChunkWidth + ", " + VoxelData.ChunkWidth);
        if (playerPos.x >= transform.position.x && playerPos.x < transform.position.x + VoxelData.ChunkWidth && playerPos.z >= transform.position.z && playerPos.z < transform.position.z + VoxelData.ChunkWidth)
        {
            return -1;
        }
        return (Mathf.Sqrt(Mathf.Pow(transform.position.x - playerPos.x, 2) + Mathf.Pow(transform.position.z - playerPos.z, 2)));
    }

    Vector3 NormalizeHit(Vector3 original, Vector3 normal) // get the hit offset to use in the array
    {
        Vector3 ret = original - this.transform.position;
        ret.x = (float)(int)(ret.x) - ((normal.x > 0) ? 1 : 0);
        ret.y = (float)(int)(ret.y) - ((normal.y > 0) ? 1 : 0);
        ret.z = (float)(int)(ret.z) - ((normal.z > 0) ? 1 : 0);

        return ret;
    }


    public bool BuildBlock(Item block, Vector3 position, Vector3 normal, bool genereateMesh = true)
    {
        Vector3 ret = position - this.transform.position;
        ret.x = (float)(int)(ret.x) - ((normal.x < 0) ? 1 : 0); // normalize ret
        ret.y = (float)(int)(ret.y) - ((normal.y < 0) ? 1 : 0);
        ret.z = (float)(int)(ret.z) - ((normal.z < 0) ? 1 : 0);

        position.x = (float)(int)(position.x) - ((normal.x < 0) ? 1 : 0); // normalize position
        position.y = (float)(int)(position.y) - ((normal.y < 0) ? 1 : 0);
        position.z = (float)(int)(position.z) - ((normal.z < 0) ? 1 : 0);



        if (block == null)
        {
            print("block is null!");
            return false;
        }
        if (ret.y < 0)
        {
            print("bellow minimum! cant place here");
            return false;
        }
        if (ret.y > VoxelData.ChunkMaxHeight)
        {
            print("Way too high! Max is 256 blocks");
            return false;
        }

        if (ret.x < 0 || ret.x >= VoxelData.ChunkWidth || ret.z < 0 || ret.z >= VoxelData.ChunkWidth)
        {
            Vector3 temp = position;
            temp.x = (float)(int)(temp.x) - ((normal.x < 0) ? 1 : 0);
            temp.y = (float)(int)(temp.y) - ((normal.y < 0) ? 1 : 0);
            temp.z = (float)(int)(temp.z) - ((normal.z < 0) ? 1 : 0);
            Chunk nxtChunk = chunkMngr.GetChunkAtPos(temp);
            if (!nxtChunk)
            {
                return false;
            }
            // print("normal: " + normal);
            ret = position - nxtChunk.gameObject.transform.position;
            ret.x = (int)(ret.x);
            ret.y = (float)(int)(ret.y) - ((normal.y < 0) ? 1 : 0);
            ret.z = (int)(ret.z);

            byte orient = 0;
            if (block.type == BlockData.BlockTypes.Log_Oak)
            {
                block.ArrLocation = 12;
                if (normal.x != 0)
                {
                    orient = 2;
                }
                else if (normal.z != 0)
                {
                    orient = 1;
                }
                block.ArrLocation += orient;
            }
            else if (block.type == BlockData.BlockTypes.Furnace)
            {
                block.ArrLocation = 21; // reset to def furnace
                float rotY = chunkMngr.MainScript.player.transform.rotation.eulerAngles.y; // get y rotation in degrees
                if (rotY > 315 || rotY < 45) // +Z
                {
                    orient = 2;
                }
                else if (Util.BetweenInc(rotY, 134, 45)) // +X -> -x
                {
                    orient = 1;
                }
                else if (Util.BetweenInc(rotY, 224, 135)) // -Z
                {
                    orient = 3;
                }
                else if (Util.BetweenInc(rotY, 315, 225)) // -X -> +x
                {
                    // default (21)
                }
                block.ArrLocation += orient;
            }
            return nxtChunk.BuildBlock(block, ret, genereateMesh);
        }

        if (ret.y >= MyChunkHeight && MyChunkHeight < VoxelData.ChunkMaxHeight && ret.y <= VoxelData.ChunkMaxHeight)
        {
            //print("adding to chunk height: " + (int)(ret.y - MyChunkHeight));
            MyChunkHeight += (int)(ret.y - (MyChunkHeight - 1));
        }

        if (voxelMap[(int)ret.x, (int)ret.y, (int)ret.z] != 0)
        {
            print("theres a block there!");
            return false;
        }

        if (block.type == BlockData.BlockTypes.Log_Oak)
        {
            block.ArrLocation = 12;
            byte orient = 0;
            if (normal.x != 0)
            {
                orient = 2;
            }
            else if (normal.z != 0)
            {
                orient = 1;
            }
            block.ArrLocation += orient;
        }
        else if (block.type == BlockData.BlockTypes.Furnace)
        {
            block.ArrLocation = 21;
            float rotY = chunkMngr.MainScript.player.transform.rotation.eulerAngles.y;
            print(rotY);
            byte orient = 0;
            if (rotY > 315 || rotY < 45) // +Z
            {
                orient = 2;
            }
            else if (Util.BetweenInc(rotY, 134, 45)) // +X -> -x
            {
                orient = 1;
            }
            else if (Util.BetweenInc(rotY, 224, 135)) // -Z
            {
                orient = 3;
            }
            else if (Util.BetweenInc(rotY, 315, 225)) // -X -> +x
            {
                // default (21)
            }
            block.ArrLocation += orient;
        }

        voxelMap[(int)ret.x, (int)ret.y, (int)ret.z] = block.ArrLocation;

        block.worldPosition = ret;
        block.ParentChunk = gameObject;

        if (block.Scripted)
        {
            voxelMapReferences[(int)ret.x, (int)ret.y, (int)ret.z] = block;
            print("pos: " + ret + ", data: " + voxelMapReferences[(int)ret.x, (int)ret.y, (int)ret.z] + ", " + voxelMapReferences[(int)ret.x, (int)ret.y, (int)ret.z].worldPosition);
        }


        if (genereateMesh)
        {
            UpdateMesh();
        }

        block.SetLight(true, position); // set light on
        block.OnPlace();
        //print("done " + voxelMap[(int)ret.x, (int)ret.y, (int)ret.z] + ", " + ret);
        return true;
    } // try build block at position with normal 
    public bool BuildBlock(Item block, Vector3 fixedPosition, bool generateMesh = true)
    {
        Vector3 ret = fixedPosition;
        //print("fixed position: " + fixedPosition + ", myPos: " + transform.position);

        if (block == null)
        {
            print("block is null!");
            return false;
        }
        if (ret.y < 0)
        {
            print("bellow minimum! cant place here");
            return false;
        }
        if (ret.y > VoxelData.ChunkMaxHeight)
        {
            print("Way too high! Max is 256 blocks");
            return false;
        }

        if (ret.x < 0 || ret.x > VoxelData.ChunkWidth || ret.z < 0 || ret.z > VoxelData.ChunkWidth)
        {
            print("out of chunk");
            // ------------------------ need to move to next chunk
            return false;
        }

        if (ret.y > MyChunkHeight - 1 && MyChunkHeight < VoxelData.ChunkMaxHeight && ret.y <= VoxelData.ChunkMaxHeight)
        {
            MyChunkHeight += (int)(ret.y - MyChunkHeight);
        }

        if (voxelMap[(int)ret.x, (int)ret.y, (int)ret.z] != 0)
        {
            print("theres a block there!");
            return false;
        }


        voxelMap[(int)ret.x, (int)ret.y, (int)ret.z] = block.ArrLocation;

        block.worldPosition = ret;
        block.ParentChunk = gameObject;

        if (block.Scripted)
        {
            voxelMapReferences[(int)ret.x, (int)ret.y, (int)ret.z] = block;
        }

        if (generateMesh)
        {
            UpdateMesh();
        }

        block.SetLight(true, gameObject.transform.position + ret); // activate light
        block.OnPlace();                                            // print("done fixed " + voxelMap[(int)ret.x, (int)ret.y, (int)ret.z] + ", " + ret);
        return true;
    } // build block at fixedPosition

    bool inVoxelMap(Vector3 pos) // if pos in voxel map
    {
        return pos.x >= 0 && pos.x < VoxelData.ChunkWidth && pos.y < VoxelData.ChunkMaxHeight && pos.y >= 0 && pos.z >= 0 && pos.z < VoxelData.ChunkWidth;
    }

    public float GetHeight(Vector2 pos) // returns height of ground at pos. returns -1 for out of bounds
    {
        pos -= new Vector2(transform.position.x, transform.position.z);
        //print(inVoxelMap(new Vector3(pos.x, 0, pos.y)) + ", " + new Vector3(pos.x, 0, pos.y) + ", " + transform.position + ", " + MyChunkHeight);
        if (!inVoxelMap(new Vector3(pos.x, 0, pos.y))) // if out of bounds
        {
            return -1;
        }

        int x, z;
        x = (int)pos.x;
        z = (int)pos.y;

        int maxY = 0;

        while (inVoxelMap(new Vector3(x, maxY, z)) && inVoxelMap(new Vector3(x, maxY + 1, z)) && voxelMap[x, maxY, z] != 0 && voxelMap[x, maxY + 1, z] != 0)
            maxY++;

        if (maxY > VoxelData.ChunkMaxHeight)
            maxY = VoxelData.ChunkMaxHeight;
        return maxY;
    }

    /*
    0 = nope 
    */
    public Item GetInteractableAtPos(Vector3 position, Vector3 normal)
    {
        Vector3 normalized = NormalizeHit(position, normal);

        if (!inVoxelMap(normalized))
        {
            return null;
        }
        if (voxelMapReferences[(int)normalized.x, (int)normalized.y, (int)normalized.z] != null && voxelMapReferences[(int)normalized.x, (int)normalized.y, (int)normalized.z].interactable)
        {
            print("pos: " + normalized + ", ref: " + voxelMapReferences[(int)normalized.x, (int)normalized.y, (int)normalized.z].type + ", " + voxelMapReferences[(int)normalized.x, (int)normalized.y, (int)normalized.z].worldPosition);
            return voxelMapReferences[(int)normalized.x, (int)normalized.y, (int)normalized.z]; // for some reason gives me a referebce to the last furnace placed
        }

        return null;
    }

    public void GenerateTrees()
    {
        int x = Random.Range(0, (int)(4 * VoxelData.TreeRarenes));
        int z = Random.Range(0, (int)(4 * VoxelData.TreeRarenes));

        if (inVoxelMap(new Vector3(x, 1, z)))
        {
            //print("gen trees " + x + ", " + z);
            int height = (int)GetHeight(new Vector2(x, z) + new Vector2(transform.position.x, transform.position.z));

            int rootHeight = Random.Range(3, 8);
            //print(rootHeight);

            int rootHead = 0;

            for (int i = 0; i < rootHeight && i < VoxelData.ChunkMaxHeight; i++)
            {
                if (inVoxelMap(new Vector3(x, 1, z)) && voxelMap[x, i + height, z] == 0)
                {
                    voxelMap[x, i + height, z] = 12;
                    rootHead = i + height;
                }
            }

            List<Vector3> leavesPos = GenerateTreeTop(new Vector3(x, rootHead, z));

            foreach (Vector3 pos in leavesPos)
            {
                if (inVoxelMap(pos - transform.position))
                {
                    Vector3 temp = pos - transform.position;
                    if (voxelMap[(int)temp.x, (int)temp.y, (int)temp.z] == 0)
                    {
                        voxelMap[(int)temp.x, (int)temp.y, (int)temp.z] = 15;
                    }
                }
                else
                {
                    Chunk chunk = chunkMngr.GetChunkAtPos(pos);
                    if (chunk)
                    {
                        Vector3 temp = pos - chunk.gameObject.transform.position;
                        if (chunk.voxelMap[(int)temp.x, (int)temp.y, (int)temp.z] == 0)
                        {
                            chunk.voxelMap[(int)temp.x, (int)temp.y, (int)temp.z] = 15;
                        }
                    }
                }
            }
            CreateMeshData();
            CreateMesh();
        }
        if (getHeight) // if getHeight is set by the chunk mngr, this chunk needs to add height to the mainScript.spawnpoint
        {
            chunkMngr.MainScript.SpawnPoint += (Vector3.up * GetHeight(new Vector2(transform.position.x, transform.position.z))); // add height
            chunkMngr.MainScript.playerController.gameObject.transform.position = chunkMngr.MainScript.SpawnPoint;// move player to position
            chunkMngr.MainScript.playerController.chunkRenderer.GetChunkDisances();
            print(chunkMngr.MainScript.SpawnPoint);
        }
        if (Last)
        {
            //chunkMngr.FinishLoading();
        }
    }

    public List<Vector3> GenerateTreeTop(Vector3 rootHead)
    {
        rootHead += transform.position;
        List<Vector3> positions = new List<Vector3>();

        int radius = Random.Range(3, 5);

        for (int i = 0; i < radius; i++) // +X
        {
            positions.Add(new Vector3(rootHead.x + i, rootHead.y, rootHead.z));

            int temp;

            for (int j = 0; j < (temp = radius - i + Random.Range(0, 2)); j++)
            {
                for (int k = 0; k < Mathf.Min(radius - j, radius - i) + 1 && i != 0; k++)
                {
                    positions.Add(new Vector3(rootHead.x + i, rootHead.y + k, rootHead.z + j));
                }
                if (j != temp - 1)
                {
                    positions.Add(new Vector3(rootHead.x + i, rootHead.y - 1, rootHead.z + j));
                }
            }
        }
        for (int i = 0; i < radius; i++) // -X
        {
            positions.Add(new Vector3(rootHead.x - i, rootHead.y, rootHead.z));

            int temp;
            for (int j = 0; j < (temp = radius - i + Random.Range(0, 2)); j++)
            {
                for (int k = 0; k < Mathf.Min(radius - j, radius - i) + 1 && i != 0; k++)
                {
                    positions.Add(new Vector3(rootHead.x + i, rootHead.y + k, rootHead.z - j));
                }
                if (j != temp - 1)
                {
                    positions.Add(new Vector3(rootHead.x + i, rootHead.y - 1, rootHead.z - j));
                }
            }
        }

        for (int i = 0; i < radius - 1; i++) // +Y
        {
            positions.Add(new Vector3(rootHead.x, rootHead.y + i, rootHead.z));
        }

        for (int i = 0; i < radius; i++) // +Z
        {
            positions.Add(new Vector3(rootHead.x, rootHead.y, rootHead.z + i));

            int temp;

            for (int j = 0; j < (temp = radius - i + Random.Range(0, 2)); j++)
            {
                for (int k = 0; k < Mathf.Min(radius - j, radius - i) + 1 && i != 0; k++)
                {
                    positions.Add(new Vector3(rootHead.x - j, rootHead.y + k, rootHead.z + i));
                }
                if (j != temp - 1)
                {
                    positions.Add(new Vector3(rootHead.x - j, rootHead.y - 1, rootHead.z + i));
                }
            }
        }
        for (int i = 0; i < radius; i++) // -Z
        {
            positions.Add(new Vector3(rootHead.x, rootHead.y, rootHead.z - i));

            int temp;

            for (int j = 0; j < (temp = radius - i + Random.Range(0, 2)); j++)
            {
                for (int k = 0; k < Mathf.Min(radius - j, radius - i) + 1; k++)
                {
                    positions.Add(new Vector3(rootHead.x - j, rootHead.y + k, rootHead.z - i));
                }
                if (j != temp - 1)
                {
                    positions.Add(new Vector3(rootHead.x - j, rootHead.y - 1, rootHead.z - i));
                }
            }
        }

        return positions;
    }

    public void UpdateMesh()
    {
        vertexIndex = 0;
        vertices.Clear();
        triangles.Clear();
        uvs.Clear();

        CreateMeshData();
        CreateMesh();
    }
}