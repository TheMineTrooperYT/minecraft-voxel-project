﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChunkRenderer : MonoBehaviour {

    public ChunkMngr chunkMngr;

    public float myTimer;


    void Update()
    {
        if (myTimer <= 0) // the render timer
        {
            GetChunkDisances(); // try to render
        }
        if (myTimer > 0) // advance timer
        {
            myTimer -= Time.deltaTime;
        }
    }

    public void GetChunkDisances()
    {
        GameObject[] chunks = chunkMngr.Chunks.ToArray(); // get array of chunks

        foreach (GameObject chunk in chunks) // run on chunks
        {
            if (chunk.GetComponent<Chunk>().GetDistance(new Vector3(transform.position.x,1,transform.position.z)) > ChunkData.ChunkRenderDistance) // if out of render distance
            {
                chunk.GetComponent<MeshRenderer>().enabled = false; // disable render
            }
            else if (!chunk.GetComponent<MeshRenderer>().enabled) // if in render distance and not disabled
            {
                chunk.GetComponent<MeshRenderer>().enabled = true; // enable render
            }
        }
        myTimer = ChunkData.ChunkRenderRefreshTimer; // reset render timer
    }
}
