﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class InventoryData
{
    public static byte InvRecipieCount = 7;
    public static byte[,,] InvRecipies = new byte[, , ]
    {
        /*
        31 32
        33 34 -> 35
         */
        { // item count, item type
            { 1, 12 }, // slot 1 (31)
            { 0, 0 }, // slot 2 (32)
            { 0, 0 }, // slot 3 (33)
            { 0, 0 }, // slot 4 (34)
            { 4, 9 }, // output (35)
        }, // planks \/
        { // item count, item type
            { 0, 0 }, // slot 1 (31)
            { 1, 12 }, // slot 2 (32)
            { 0, 0 }, // slot 3 (33)
            { 0, 0 }, // slot 4 (34)
            { 4, 9 }, // output (35)
        },
        { // item count, item type
            { 0, 0 }, // slot 1 (31)
            { 0, 0 }, // slot 2 (32)
            { 1, 12 }, // slot 3 (33)
            { 0, 0 }, // slot 4 (34)
            { 4, 9 }, // output (35)
        },
        { // item count, item type
            { 0, 0 }, // slot 1 (31)
            { 0, 0 }, // slot 2 (32)
            { 0, 0 }, // slot 3 (33)
            { 1, 12 }, // slot 4 (34)
            { 4, 9 }, // output (35)
        }, // planks /\ oak
        { // item count, item type
            { 1, 9 }, // slot 1 (31)
            { 1, 9 }, // slot 2 (32)
            { 1, 9 }, // slot 3 (33)
            { 1, 9 }, // slot 4 (34)
            { 1, 8 }, // output (35)
        }, // crafting table
        {
            { 0, 0 },
            { 1, 9 },//
            { 0, 0 },
            { 1, 9 },//
            { 4, 18 },
        },
        {
            { 1, 9 },//
            { 0, 0 },
            { 1, 9 },//
            { 0, 0 },
            { 4, 18 },
        }
    };

    public static byte TableRecipieCount = 23;
    public static byte[,,] TableRecipies = new byte[, , ]
    {
        /*
         0 3 6
         1 4 7 -> 9 
         2 5 8 
         */
        {//item count, item type
            {1,12 },//0
            {0,0 },//1
            {0,0 },//2

            {0,0 },//3
            {0,0 },//4
            {0,0 },//5

            {0,0 },//6
            {0,0 },//7
            {0,0 },//8

            {4,9 },//9 output
        }, // start: log to planks
        {
            {0,0 },//0
            {1,12 },//1
            {0,0 },//2
            {0,0 },//3
            {0,0 },//4
            {0,0 },//5
            {0,0 },//6
            {0,0 },//7
            {0,0 },//8
            {4,9 },//9 output
        },
        {
            {0,0 },//1
            {0,0 },//2
            {1,12 },//0
            {0,0 },//3
            {0,0 },//4
            {0,0 },//5
            {0,0 },//6
            {0,0 },//7
            {0,0 },//8
            {4,9 },//9 output
        },
        {
            {0,0 },//1
            {0,0 },//2
            {0,0 },//3
            {1,12 },//0
            {0,0 },//4
            {0,0 },//5
            {0,0 },//6
            {0,0 },//7
            {0,0 },//8
            {4,9 },//9 output
        },
        {
            {0,0 },//1
            {0,0 },//2
            {0,0 },//3
            {0,0 },//4
            {1,12 },//0
            {0,0 },//5
            {0,0 },//6
            {0,0 },//7
            {0,0 },//8
            {4,9 },//9 output
        },
        {
            {0,0 },//1
            {0,0 },//2
            {0,0 },//3
            {0,0 },//4
            {0,0 },//5
            {1,12 },//0
            {0,0 },//6
            {0,0 },//7
            {0,0 },//8
            {4,9 },//9 output
        },
        {
            {0,0 },//1
            {0,0 },//2
            {0,0 },//3
            {0,0 },//4
            {0,0 },//5
            {0,0 },//6
            {1,12 },//0
            {0,0 },//7
            {0,0 },//8
            {4,9 },//9 output
        },
        {
            {0,0 },//1
            {0,0 },//2
            {0,0 },//3
            {0,0 },//4
            {0,0 },//5
            {0,0 },//6
            {0,0 },//7
            {1,12 },//0
            {0,0 },//8
            {4,9 },//9 output
        },
        {
            {0,0 },//1
            {0,0 },//2
            {0,0 },//3
            {0,0 },//4
            {0,0 },//5
            {0,0 },//6
            {0,0 },//7
            {0,0 },//8
            {1,12 },//0
            {4,9 },//9 output
        }, // end;
        {
            { 1, 9 },
            { 1, 9 },
            {0,0 },
            { 1, 9 },
            { 1, 9 },
            {0,0 },
            {0,0 },
            {0,0 },
            {0,0 },
            { 1, 8 },
        }, // start: crafting_table
        {
            {0,0 },
            { 1, 9 },
            { 1, 9 },
            {0,0 },
            { 1, 9 },
            { 1, 9 },
            {0,0 },
            {0,0 },
            {0,0 },
            { 1, 8 },
        },
        {
            {0,0 },
            {0,0 },
            {0,0 },
            { 1, 9 },
            { 1, 9 },
            {0,0 },
            { 1, 9 },
            { 1, 9 },
            {0,0 },
            { 1, 8 },
        },
        {
            {0,0 },
            {0,0 },
            {0,0 },

            {0,0 },
            { 1, 9 },
            { 1, 9 },

            {0,0 },
            { 1, 9 },
            { 1, 9 },

            { 1, 8 },
        }, // end;
        {
            {0,0 },
            {0,0 },
            {0,0 },

            {0,0 },
            { 1, 9 },
            { 1, 9 },

            {0,0 },
            {0,0 },
            {0,0 },

            { 4, 18 },
        },// start: planks to sticks
        {
            {0,0 },
            {0,0 },
            {0,0 },

            { 1, 9 },
            { 1, 9 },
            {0,0 },

            {0,0 },
            {0,0 },
            {0,0 },

            { 4, 18 },
        },
        {
            {0,0 },
            { 1, 9 },
            { 1, 9 },

            {0,0 },
            {0,0 },
            {0,0 },

            {0,0 },
            {0,0 },
            {0,0 },

            { 4, 18 },
        },
        {
            { 1, 9 },
            { 1, 9 },
            {0,0 },

            {0,0 },
            {0,0 },
            {0,0 },

            {0,0 },
            {0,0 },
            {0,0 },

            { 4, 18 },
        },
        {
            {0,0 },
            {0,0 },
            {0,0 },

            {0,0 },
            {0,0 },
            {0,0 },

            {0,0 },
            { 1, 9 },
            { 1, 9 },

            { 4, 18 },
        },
        {
            {0,0 },
            {0,0 },
            {0,0 },

            {0,0 },
            {0,0 },
            {0,0 },

            { 1, 9 },
            { 1, 9 },
            {0,0 },

            { 4, 18 },
        },// end;
        {
            { 1, 6 },//c
            { 0, 0 },
            { 0, 0 },

            { 1, 6 },//c
            { 1, 18 },//s
            { 1, 18 },//s
            
            { 1, 6 },//c
            { 0, 0 },
            { 0, 0 },

            { 1, 19 },//pick stone
        }, // stone pick
        {
            { 1, 6 },//c
            { 1, 6 }, // c
            { 0, 0 },

            { 1, 6 },//c
            { 1, 18 },//s
            { 1, 18 },//s
            
            { 0, 0 },
            { 0, 0 },
            { 0, 0 },

            { 1, 20 },//axe stone
        }, // stone axe1
        {
            { 0, 0 },
            { 0, 0 },
            { 0, 0 },

            { 1, 6 },//c
            { 1, 6 }, // c
            { 0, 0 },

            { 1, 6 },//c
            { 1, 18 },//s
            { 1, 18 },//s
            


            { 1, 20 },//axe stone
        }, // stone axe2
        {
            { 1, 6 },
            { 1, 6 },
            { 1, 6 },
            { 1, 6 },
            { 0, 0 },
            { 1, 6 },
            { 1, 6 },
            { 1, 6 },
            { 1, 6 },
            { 1, 21 },
        } // furnce
    };

}

public class Inventory : MonoBehaviour
{
    public Player MainScript;

    public Texture[] Blocks;
    public BlockData.BlockTypes[] blockTypes; // block types corresponding to the block texture list
    public Material[] droppedBlock;

    public Texture[] Parts;
    public BlockData.PartTypes[] partTypes; // block types corresponding to the block texture list
    public Material[] droppedPart;

    public GameObject DropItem;


    public int inventoryBarSize = 9;
    public int inventoryBackpackSize = 36;// 0-26 backpack,27-30 armor,31-34 crafting table,35 crafting output
    public int inventoryBackpackSize_ExcludingSpecials = 27;
    public int inventorySlotSize = 64;
    public Stack<Item>[] inventoryBarSpace = new Stack<Item>[9]; // bar blocks
    public RawImage[] inventoryBar = new RawImage[9]; // bar images
    public Stack<Item>[] inventoryBackpackSpace = new Stack<Item>[36]; // backpack blocks
    public RawImage[] inventoryBackpack = new RawImage[36]; // backpack images

    public Slider[] inventoryBarDurabilityViewer = new Slider[9];
    public Slider[] inventoryBackpackDurabilityViewer = new Slider[36];

    public int CraftingTableSize = 10;
    public Stack<Item>[] CraftingTableSpace = new Stack<Item>[10];
    public RawImage[] CraftingTable = new RawImage[10];
    public Slider[] CraftingTableDurabilityViewer = new Slider[10];

    public int FurnaceSize = 3;
    public Item[] FurnaceSpace = new Item[3];
    public int[] FurnaceSpaceCounter = new int[3];
    public RawImage[] Furnace = new RawImage[3];
    public Slider[] FurnaceDurabilityViewer = new Slider[3];
    public Slider[] FurnaceProggressSliders = new Slider[2];
    public FurnaceUpdater OpenFurnace = null;

    public RawImage[] BarSelectionView = new RawImage[9]; // bar selection viewer images
    public Texture SelectionViewTexture; // bar selection view image

    public GameObject[] inventoryBackpackGameObj = new GameObject[3];

    public RawImage Held_Item;
    public Stack<Item> HeldItem_block;
    public Slider HeldItem_DurabilitySlider;

    public GameObject SelectedItemDisplay;

    public GameObject[] BlockGUIs = new GameObject[1];

    public int Location; // the inventory bar location

    public bool isBackpackOpen;
    public bool isEscapeMenu;

    public bool HeldItem_isBackpack;
    public bool HeldItem_isBar;
    public int HeldItem_location;

    public bool checkCrafting = false;
    public int CurrentInvCraftingRecipeIndex = -1;

    public bool isCraftingOpen = false;
    public bool checkTableCrafting = false;
    public int CurrentTableRecipeIndex = -1;

    public Item HeldToolReference; // 0 for non tool

    public Item OpenGuiBlock = null;

    void Start()
    {
        HeldItem_block = new Stack<Item>();
        Location = 0;
        for (int i = 0; i < inventoryBackpackSize; i++)
            inventoryBackpackSpace[i] = new Stack<Item>();
        for (int i = 0; i < inventoryBarSize; i++)
            inventoryBarSpace[i] = new Stack<Item>();
        for (int i = 0; i < CraftingTableSize; i++)
            CraftingTableSpace[i] = new Stack<Item>();

        Location = 0;
        for (int i = 0; i < inventoryBarSize; i++)
        {
            if (i != Location)
            {
                BarSelectionView[i].texture = Blocks[0];
            }
            else
            {
                BarSelectionView[i].texture = SelectionViewTexture;
            }
        } // reset bar viewer
        addBlock(new Tool_Pickaxe_Iron(), true);
        CloseAllGUIs();
        UpdateGraphics();
        isBackpackOpen = false;
        print("created inventory");
    }

    void Update()
    {
        float move;
        if ((move = -Input.GetAxis("Mouse ScrollWheel")) != 0f && !MainScript.Dead && !isBackpackOpen) // if mouse scroll wheel
        {
            if (move < 0 && Location - (move * -10) > -1)
            {
                Location -= (int)(move * -10);
                for (int i = 0; i < inventoryBarSize; i++)
                {
                    if (i != Location)
                    {
                        BarSelectionView[i].texture = Blocks[0];
                    }
                    else
                    {
                        BarSelectionView[i].texture = SelectionViewTexture;
                    }
                }
                if (inventoryBarSpace[Location].Count > 0)
                {
                    HeldToolReference = inventoryBarSpace[Location].Peek();
                }
                else
                {
                    HeldToolReference = null;
                }
            }
            else if (move > 0 && Location + (move * 10) < 9)
            {
                Location += (int)(move * 10);
                for (int i = 0; i < inventoryBarSize; i++)
                {
                    if (i != Location)
                    {
                        BarSelectionView[i].texture = Blocks[0];
                    }
                    else
                    {
                        BarSelectionView[i].texture = SelectionViewTexture;
                    }
                }
                if (inventoryBarSpace[Location].Count > 0)
                {
                    HeldToolReference = inventoryBarSpace[Location].Peek();
                }
                else
                {
                    HeldToolReference = null;
                }
            }
        }
        if (Input.GetKeyDown("0") || Input.GetKeyDown("1") && !MainScript.Dead)
        {
            Location = 0;
            for (int i = 0; i < inventoryBarSize; i++)
            {
                if (i != Location)
                {
                    BarSelectionView[i].texture = Blocks[0];
                }
                else
                {
                    BarSelectionView[i].texture = SelectionViewTexture;
                }
            }
        } // if any of the keys is pressed move location to the corresponding locatio and update the viewer
        if (Input.GetKeyDown("2") && !MainScript.Dead)
        {
            Location = 1;
            for (int i = 0; i < inventoryBarSize; i++)
            {
                if (i != Location)
                {
                    BarSelectionView[i].texture = Blocks[0];
                }
                else
                {
                    BarSelectionView[i].texture = SelectionViewTexture;
                }
            }
        }
        if (Input.GetKeyDown("3") && !MainScript.Dead)
        {
            Location = 2;
            for (int i = 0; i < inventoryBarSize; i++)
            {
                if (i != Location)
                {
                    BarSelectionView[i].texture = Blocks[0];
                }
                else
                {
                    BarSelectionView[i].texture = SelectionViewTexture;
                }
            }
        }
        if (Input.GetKeyDown("4") && !MainScript.Dead)
        {
            Location = 3;
            for (int i = 0; i < inventoryBarSize; i++)
            {
                if (i != Location)
                {
                    BarSelectionView[i].texture = Blocks[0];
                }
                else
                {
                    BarSelectionView[i].texture = SelectionViewTexture;
                }
            }
        }
        if (Input.GetKeyDown("5") && !MainScript.Dead)
        {
            Location = 4;
            for (int i = 0; i < inventoryBarSize; i++)
            {
                if (i != Location)
                {
                    BarSelectionView[i].texture = Blocks[0];
                }
                else
                {
                    BarSelectionView[i].texture = SelectionViewTexture;
                }
            }
        }
        if (Input.GetKeyDown("6") && !MainScript.Dead)
        {
            Location = 5;
            for (int i = 0; i < inventoryBarSize; i++)
            {
                if (i != Location)
                {
                    BarSelectionView[i].texture = Blocks[0];
                }
                else
                {
                    BarSelectionView[i].texture = SelectionViewTexture;
                }
            }
        }
        if (Input.GetKeyDown("7") && !MainScript.Dead)
        {
            Location = 6;
            for (int i = 0; i < inventoryBarSize; i++)
            {
                if (i != Location)
                {
                    BarSelectionView[i].texture = Blocks[0];
                }
                else
                {
                    BarSelectionView[i].texture = SelectionViewTexture;
                }
            }
        }
        if (Input.GetKeyDown("8") && !MainScript.Dead)
        {
            Location = 7;
            for (int i = 0; i < inventoryBarSize; i++)
            {
                if (i != Location)
                {
                    BarSelectionView[i].texture = Blocks[0];
                }
                else
                {
                    BarSelectionView[i].texture = SelectionViewTexture;
                }
            }
        }
        if (Input.GetKeyDown("9") && !MainScript.Dead)
        {
            Location = 8;
            for (int i = 0; i < inventoryBarSize; i++)
            {
                if (i != Location)
                {
                    BarSelectionView[i].texture = Blocks[0];
                }
                else
                {
                    BarSelectionView[i].texture = SelectionViewTexture;
                }
            }
        }

        if (isBackpackOpen && !inventoryBackpackGameObj[0].activeSelf && !isEscapeMenu && !MainScript.Dead)
        {
            for (int i = 0; i < inventoryBackpackGameObj.Length; i++)
            {
                inventoryBackpackGameObj[i].SetActive(true);
            }
        } // open inventory
        else if (!isBackpackOpen && inventoryBackpackGameObj[0].activeSelf || isEscapeMenu)
        {
            for (int i = 0; i < inventoryBackpackGameObj.Length; i++)
            {
                inventoryBackpackGameObj[i].SetActive(false);
            }
            CloseAllGUIs();
            ClearCraftingTable();
        } // close inventory

        if (!isBackpackOpen && Held_Item.gameObject.activeSelf)
        {
            if (HeldItem_isBackpack) // in case that i have held item
            {
                while (HeldItem_block.Count > 0) // if in backpack, move the stack to backpackSpace[held location]
                {
                    inventoryBackpackSpace[HeldItem_location].Push(HeldItem_block.Pop());
                }
                UpdateGraphics();
                Held_Item.gameObject.SetActive(false);
            }
            else if (HeldItem_isBar)
            {
                while (HeldItem_block.Count > 0) // if in bar, move the stack to barSpace[held location]
                {
                    inventoryBarSpace[HeldItem_location].Push(HeldItem_block.Pop());
                }
                UpdateGraphics();
                Held_Item.gameObject.SetActive(false);
            }
            else if (HeldItem_block.Count > 0)
            {
                DropItemFunc(MainScript.player.transform.position, HeldItem_block.Peek(), -1, HeldItem_block.Count);
                while (HeldItem_block.Count > 0)
                {
                    HeldItem_block.Pop();
                }
            }
        }

        if (HeldItem_block.Count > 0 && !Held_Item.gameObject.activeSelf && !MainScript.Dead)
        {
            Held_Item.gameObject.SetActive(true);
        } // set active for the held item
        else if (HeldItem_block.Count == 0 && Held_Item.gameObject.activeSelf && !MainScript.Dead)
        {
            Held_Item.gameObject.SetActive(false);
        } // disable the held item if empty

        if (HeldItem_block.Count > 0 && isBackpackOpen)
        {
            Held_Item.gameObject.transform.position = Input.mousePosition;
        } // if held item, move it to mouse position

        if (Input.GetKeyDown(KeyCode.Q) && !MainScript.Dead)
        {
            //print("pressed q");
            if (isBackpackOpen)
            {

            }
            else
            {
                //print("in bar " + Location + ", " + inventoryBarSpace[Location]);
                if (inventoryBarSpace[Location].Count > 0)
                {
                    DropItemFunc(MainScript.player.transform.position + Vector3.up, inventoryBarSpace[Location].Peek());
                    inventoryBarSpace[Location].Pop();
                    //ztemp_script.GetComponent<Rigidbody>().velocity = MainScript.player.GetComponent<CameraScript>().gravity.velocity*2;
                    UpdateGraphics();
                }
            }
        } // drop item

        if (inventoryBarSpace[Location].Count > 0 && !SelectedItemDisplay.activeSelf || (inventoryBarSpace[Location].Count > 0 && SelectedItemDisplay.GetComponent<SelectedItem>().type != inventoryBarSpace[Location].Peek().type))
        {
            SelectedItemDisplay.SetActive(true);

            Material temp_mat = null;
            int i;
            for (i = 0; i < blockTypes.Length; i++)
            {
                if (blockTypes[i] == inventoryBarSpace[Location].Peek().type && blockTypes[i] != BlockData.BlockTypes.NONE && blockTypes[i] != BlockData.BlockTypes.Tool)
                {
                    temp_mat = droppedBlock[i];
                    break;
                }
            }

            if (i == blockTypes.Length)
            {
                for (i = 0; i < partTypes.Length; i++)
                {
                    if (partTypes[i] == inventoryBarSpace[Location].Peek().partType)
                    {
                        temp_mat = droppedPart[i];
                        break;
                    }
                }
            }

            SelectedItemDisplay.GetComponent<SelectedItem>().rend.material = temp_mat;
            SelectedItemDisplay.GetComponent<SelectedItem>().type = inventoryBarSpace[Location].Peek().type;
        }
        if (inventoryBarSpace[Location].Count == 0 && SelectedItemDisplay.activeSelf)
        {
            SelectedItemDisplay.SetActive(false);
        } // deal with selectedItemDisplay (set active/inacitve, set textures)
    }

    public bool addBlock(Item block, bool override_creative = false)
    {
        if (block != null) // if got block
        {
            int idx = findBlock_bar(block.type); // try to find in bar
            int i = -1;
            if (idx != -1 && inventoryBarSpace[idx].Count < inventoryBarSpace[idx].Peek().StackSize) // if found and stack got space
            {
                if (Data.GameMode_int != 1 || override_creative) // if not in creative or ovveride_creative
                {
                    inventoryBarSpace[idx].Push(block); // add block
                    UpdateGraphics(); // update graphics
                }
                return true; // return success
            }
            else // if not found in bar
            {
                for (i = 0; i < inventoryBarSize; i++) // find empty slot in bar
                {
                    if (inventoryBarSpace[i].Count == 0) // if found
                    {
                        inventoryBarSpace[i].Push(block); // add block
                        UpdateGraphics(); // updateGraphics
                        return true;//return success
                    }
                }
                if (i == inventoryBarSize) // bar is full
                {
                    idx = findBlock_backpack(block.type); // try to find in backpack
                    if (idx != -1 && inventoryBackpackSpace[idx].Count < inventoryBackpackSpace[idx].Peek().StackSize) // if found
                    {
                        if (Data.GameMode_int != 1 || override_creative) // if not in creative or ovveride_creative
                        {
                            inventoryBackpackSpace[idx].Push(block);//add block
                            UpdateGraphics(); // update graphics
                        }
                        return true; // return success
                    }
                    for (i = 0; i < inventoryBackpackSize_ExcludingSpecials; i++) // if not found
                    {
                        if (inventoryBackpackSpace[i].Count == 0) //find empty slot
                        {
                            inventoryBackpackSpace[i].Push(block); // add block
                            UpdateGraphics(); // update graphics
                            return true; // return success
                        }
                    }
                }
            }
            UpdateGraphics(); // update graphics
            return false; // return fail
        }
        else // block is null
        {
            Debug.Log("inventory addBlock got null");
        }

        return false; // return fail
    } // add block to inventory

    public Item removeBlock() //remove selected block
    {
        Item ret = null;

        if (inventoryBarSpace[Location].Count > 0)
        {
            ret = inventoryBarSpace[Location].Pop();
        }
        else
        {
            Debug.Log("tried to remove empty location!");
        }
        UpdateGraphics();
        return ret; // return block. if found, the value is the block removed, if not value is null
    }

    public Item GetHeldBlock() // get a reference to held block (held: location)
    {
        if (inventoryBarSpace[Location].Count > 0)
        {
            return inventoryBarSpace[Location].Peek();
        }

        return null;
    }

    private int findBlock_bar(BlockData.BlockTypes type) //  returns at which cell the block type is (-1 for not found) (at bar only!)
    {
        for (int i = inventoryBarSize - 1; i >= 0; i--)
        {
            if (inventoryBarSpace[i].Count > 0 && inventoryBarSpace[i].Count < inventoryBarSpace[i].Peek().StackSize && inventoryBarSpace[i].Peek().type == type)
            {
                return i;
            }
        }
        return -1;
    }
    private int findBlock_backpack(BlockData.BlockTypes type) //  returns at which cell the block type is (-1 for not found) (at backpack only!)
    {
        for (int i = 0; i < inventoryBackpackSize; i++)
        {
            if ((i > Data.inventoryArmorHelmetVal || i < Data.inventoryArmorBootsVal) && (i > Data.inventoryCraftingTableTopVal || i < Data.inventoryCraftingTableBottomVal) && i != Data.inventoryCraftingOutput && inventoryBackpackSpace[i].Count > 0 && inventoryBackpackSpace[i].Count < inventoryBackpackSpace[i].Peek().StackSize && inventoryBackpackSpace[i].Peek().type == type)
            {
                return i;
            }
        }
        return -1;
    }



    public void ClickedInvButton(int id, bool inBackpack = false, bool inCraftingTable = false, bool inFurnace = false)// on left click on id
    {
        if (isBackpackOpen)
        {
            //print("clicked button id: " + id  + ", in backpack: " + inBackpack);
            if (inBackpack) // backpack
            {
                if (inventoryBackpackSpace[id].Count > 0 && HeldItem_block.Count == 0)
                {
                    while (inventoryBackpackSpace[id].Count > 0)
                    {
                        HeldItem_block.Push(inventoryBackpackSpace[id].Pop());
                    }
                    if (id == inventoryBackpackSize - 1)
                    {
                        int slot_index = 31;
                        for (int j = 0; j < 4; j++)
                        {
                            for (int k = 0; k < InventoryData.InvRecipies[CurrentInvCraftingRecipeIndex, j, 0] && inventoryBackpackSpace[slot_index].Count > 0; k++)
                            {
                                inventoryBackpackSpace[slot_index].Pop();
                            }
                            slot_index++;
                        }
                    }
                } // if item in selected slot and none held
                else if (inventoryBackpackSpace[id].Count > 0 && HeldItem_block.Count > 0)
                {
                    if (inventoryBackpackSpace[id].Peek().type == HeldItem_block.Peek().type && IsItemAllowedInLocationBackpack(HeldItem_block.Peek(), id) && HeldItem_block.Peek().type != BlockData.BlockTypes.Tool) // if selected block is from same type
                    {
                        int stackSize = inventoryBackpackSpace[id].Peek().StackSize; // get the item stack size
                        while (inventoryBackpackSpace[id].Count < stackSize && HeldItem_block.Count > 0) // add to location till stackSize
                        {
                            inventoryBackpackSpace[id].Push(HeldItem_block.Pop());
                        }
                    }
                    else
                    {
                        if (IsItemAllowedInLocationBackpack(HeldItem_block.Peek(), id))
                        {
                            Stack<Item> temp = new Stack<Item>(); // switch between held and location
                            while (HeldItem_block.Count > 0)
                            {
                                temp.Push(HeldItem_block.Pop());
                            }
                            while (inventoryBackpackSpace[id].Count > 0)
                            {
                                HeldItem_block.Push(inventoryBackpackSpace[id].Pop());
                            }
                            while (temp.Count > 0)
                            {
                                inventoryBackpackSpace[id].Push(temp.Pop());
                            }
                        }
                    }
                } // if item in slot and held
                else if (inventoryBackpackSpace[id].Count == 0 && HeldItem_block.Count > 0)
                {
                    if (IsItemAllowedInLocationBackpack(HeldItem_block.Peek(), id))
                    {
                        while (HeldItem_block.Count > 0)
                        {
                            inventoryBackpackSpace[id].Push(HeldItem_block.Pop());
                        }
                    }
                } // if none in spot and held
                if (InInvCraftingArea(id))
                {
                    CheckInvCrafting();
                }
            }
            else if (!inCraftingTable && !inFurnace)// bar
            {
                if (inventoryBarSpace[id].Count > 0 && HeldItem_block.Count == 0)
                {
                    while (inventoryBarSpace[id].Count > 0)
                    {
                        HeldItem_block.Push(inventoryBarSpace[id].Pop());
                    }
                } // if item in selected slot and none held
                else if (inventoryBarSpace[id].Count > 0 && HeldItem_block.Count > 0)
                {
                    if (inventoryBarSpace[id].Peek().type == HeldItem_block.Peek().type) // if same type
                    {
                        int stackSize = inventoryBarSpace[id].Peek().StackSize; // get block stack size
                        while (inventoryBarSpace[id].Count < stackSize && HeldItem_block.Count > 0) // add to location till stack size
                        {
                            inventoryBarSpace[id].Push(HeldItem_block.Pop());
                        }
                    }
                    else
                    {
                        Stack<Item> temp = new Stack<Item>(); // switch between location and held
                        while (HeldItem_block.Count > 0)
                        {
                            temp.Push(HeldItem_block.Pop());
                        }
                        while (inventoryBarSpace[id].Count > 0)
                        {
                            HeldItem_block.Push(inventoryBarSpace[id].Pop());
                        }
                        while (temp.Count > 0)
                        {
                            inventoryBarSpace[id].Push(temp.Pop());
                        }
                    }

                } // if in selected and held
                else if (inventoryBarSpace[id].Count == 0 && HeldItem_block.Count > 0)
                {
                    while (HeldItem_block.Count > 0)
                    {
                        inventoryBarSpace[id].Push(HeldItem_block.Pop());
                    }
                } // if none in selected and held
            }
            else if (inCraftingTable)
            {
                if (CraftingTableSpace[id].Count > 0 && HeldItem_block.Count == 0)
                {
                    while (CraftingTableSpace[id].Count > 0)
                    {
                        HeldItem_block.Push(CraftingTableSpace[id].Pop());
                    }
                    if (id == Data.CraftingTableOutput)
                    {
                        print(CurrentTableRecipeIndex);
                        for (int j = 0; j < 9; j++)
                        {
                            for (int k = 0; k < InventoryData.TableRecipies[CurrentTableRecipeIndex, j, 0] && CraftingTableSpace[j].Count > 0; k++)
                            {
                                CraftingTableSpace[j].Pop();
                            }
                        }
                    }
                } // if item in selected slot and none held
                else if (CraftingTableSpace[id].Count > 0 && HeldItem_block.Count > 0 && id != Data.CraftingTableOutput)
                {
                    if (CraftingTableSpace[id].Peek().type == HeldItem_block.Peek().type) // if same type
                    {
                        int stackSize = CraftingTableSpace[id].Peek().StackSize; // get block stack size
                        while (CraftingTableSpace[id].Count < stackSize && HeldItem_block.Count > 0) // add to location till stack size
                        {
                            CraftingTableSpace[id].Push(HeldItem_block.Pop());
                        }
                    }
                    else
                    {
                        Stack<Item> temp = new Stack<Item>(); // switch between location and held
                        while (HeldItem_block.Count > 0)
                        {
                            temp.Push(HeldItem_block.Pop());
                        }
                        while (inventoryBarSpace[id].Count > 0)
                        {
                            HeldItem_block.Push(CraftingTableSpace[id].Pop());
                        }
                        while (temp.Count > 0)
                        {
                            CraftingTableSpace[id].Push(temp.Pop());
                        }
                    }

                } // if in selected and held
                else if (CraftingTableSpace[id].Count == 0 && HeldItem_block.Count > 0 && id < CraftingTableSize - 1)
                {
                    while (HeldItem_block.Count > 0)
                    {
                        CraftingTableSpace[id].Push(HeldItem_block.Pop());
                    }
                } // if none in selected and held
                CheckTableCrafting();
            }
            else if (inFurnace)
            {
                if (FurnaceSpace[id] != null && HeldItem_block.Count == 0)
                {
                    while (FurnaceSpaceCounter[id] > 0)
                    {
                        HeldItem_block.Push(FurnaceSpace[id]);
                        FurnaceSpaceCounter[id]--;
                    }
                    FurnaceSpace[id] = null;
                } // if item in selected slot and none held
                else if (FurnaceSpace[id] != null && HeldItem_block.Count > 0)
                {
                    if (FurnaceSpace[id].type == HeldItem_block.Peek().type && IsItemAllowedInLocation_Furnace(id, HeldItem_block.Peek())) // if same type
                    {
                        int stackSize = FurnaceSpace[id].StackSize; // get block stack size
                        while (FurnaceSpaceCounter[id] < stackSize && HeldItem_block.Count > 0) // add to location till stack size
                        {
                            FurnaceSpaceCounter[id]++;
                            FurnaceSpace[id] = HeldItem_block.Pop();
                        }
                    }
                    else
                    {
                        if (IsItemAllowedInLocation_Furnace(id, HeldItem_block.Peek()))
                        {
                            Stack<Item> temp = new Stack<Item>(); // switch between location and held
                            while (HeldItem_block.Count > 0)
                            {
                                temp.Push(HeldItem_block.Pop());
                            }
                            while (FurnaceSpaceCounter[id] > 0)
                            {
                                HeldItem_block.Push(FurnaceSpace[id]);
                                FurnaceSpaceCounter[id]--;
                            }
                            while (temp.Count > 0)
                            {
                                FurnaceSpace[id] = temp.Pop();
                                FurnaceSpaceCounter[id]++;
                            }
                        }
                    } // item in slot


                } // if in selected and held
                else if (FurnaceSpaceCounter[id] == 0 && HeldItem_block.Count > 0 && IsItemAllowedInLocation_Furnace(id, HeldItem_block.Peek()))
                {
                    while (HeldItem_block.Count > 0)
                    {
                        FurnaceSpace[id] = HeldItem_block.Pop();
                        FurnaceSpaceCounter[id]++;
                    } // item slot

                } // if none in selected and held
            }
            UpdateGraphics();

            if (HeldItem_block.Count > 0) // if holding something, save location and inBackpack
            {
                HeldItem_isBackpack = inBackpack;
                HeldItem_isBar = !inCraftingTable && !inFurnace && !inBackpack;
                HeldItem_location = id;
            }
        }
    }
    public void RightClickedInvButton(int id, bool inBackpack = false, bool inCraftingTable = false, bool inFurnace = false) // on right click on id
    {
        if (isBackpackOpen)
        {
            if (inBackpack) // backpack
            {
                if (inventoryBackpackSpace[id].Count > 0 && HeldItem_block.Count == 0)
                {
                    if (id == inventoryBackpackSize - 1)
                    {
                        while (inventoryBackpackSpace[id].Count > 0)
                        {
                            HeldItem_block.Push(inventoryBackpackSpace[id].Pop());
                        }
                        if (id == inventoryBackpackSize - 1)
                        {
                            int slot_index = Data.inventoryCraftingTableBottomVal;
                            for (int j = 0; j < Data.inventoryCraftingTableTopVal - Data.inventoryCraftingTableBottomVal + 1; j++)
                            {
                                for (int k = 0; k < InventoryData.InvRecipies[CurrentInvCraftingRecipeIndex, j, 0] && inventoryBackpackSpace[slot_index].Count > 0; k++)
                                {
                                    inventoryBackpackSpace[slot_index].Pop();
                                }
                                slot_index++;
                            }
                        }
                    }
                    else
                    {
                        int count = inventoryBackpackSpace[id].Count / 2;
                        while (inventoryBackpackSpace[id].Count > 0 && count > 0)
                        {
                            HeldItem_block.Push(inventoryBackpackSpace[id].Pop());
                            count--;
                        }
                    }

                } // if item in selected slot and none held
                else if (inventoryBackpackSpace[id].Count > 0 && HeldItem_block.Count > 0)
                {
                    if (inventoryBackpackSpace[id].Peek().type == HeldItem_block.Peek().type && IsItemAllowedInLocationBackpack(HeldItem_block.Peek(), id)) // if selected block is from same type
                    {
                        int stackSize = inventoryBackpackSpace[id].Peek().StackSize; // get the item stack size
                        if (inventoryBackpackSpace[id].Count < stackSize && HeldItem_block.Count > 0) // add to location till stackSize
                        {
                            inventoryBackpackSpace[id].Push(HeldItem_block.Pop());
                        }
                    }
                } // if item in slot and held
                else if (inventoryBackpackSpace[id].Count == 0 && HeldItem_block.Count > 0)
                {
                    if (IsItemAllowedInLocationBackpack(HeldItem_block.Peek(), id))
                    {
                        if (HeldItem_block.Count > 0)
                        {
                            inventoryBackpackSpace[id].Push(HeldItem_block.Pop());
                        }
                    }
                } // if none in spot and held
                if (InInvCraftingArea(id))
                {
                    print("CheckInvCrafting()");
                    CheckInvCrafting();
                }
            }
            else if (!inCraftingTable && !inFurnace) // bar
            {
                if (inventoryBarSpace[id].Count > 0 && HeldItem_block.Count == 0)
                {
                    int count = inventoryBarSpace[id].Count;
                    if (count > 1)
                    {
                        count /= 2;
                    }
                    while (inventoryBarSpace[id].Count > 0 && count > 0)
                    {
                        HeldItem_block.Push(inventoryBarSpace[id].Pop());
                        count--;
                    }
                } // if item in selected slot and none held
                else if (inventoryBarSpace[id].Count > 0 && HeldItem_block.Count > 0)
                {
                    if (inventoryBarSpace[id].Peek().type == HeldItem_block.Peek().type && IsItemAllowedInLocationBackpack(HeldItem_block.Peek(), id)) // if selected block is from same type
                    {
                        int stackSize = inventoryBarSpace[id].Peek().StackSize; // get the item stack size
                        if (inventoryBarSpace[id].Count < stackSize && HeldItem_block.Count > 0) // add to location till stackSize
                        {
                            inventoryBarSpace[id].Push(HeldItem_block.Pop());
                        }
                    }
                } // if in selected and held
                else if (inventoryBarSpace[id].Count == 0 && HeldItem_block.Count > 0)
                {
                    if (HeldItem_block.Count > 0)
                    {
                        inventoryBarSpace[id].Push(HeldItem_block.Pop());
                    }
                } // if none in selected and held
            }
            else if (inCraftingTable)
            {
                if (CraftingTableSpace[id].Count > 0 && HeldItem_block.Count == 0)
                {
                    if (id == Data.CraftingTableOutput)
                    {
                        while (CraftingTableSpace[id].Count > 0)
                        {
                            HeldItem_block.Push(CraftingTableSpace[id].Pop());
                        }
                        int slot_index = 0;
                        for (int j = 0; j < 9; j++)
                        {
                            for (int k = 0; k < InventoryData.InvRecipies[CurrentTableRecipeIndex, j, 0] && CraftingTableSpace[slot_index].Count > 0; k++)
                            {
                                CraftingTableSpace[slot_index].Pop();
                            }
                            slot_index++;
                        }

                    }
                    else
                    {
                        int count = CraftingTableSpace[id].Count / 2;
                        if (count == 0 && CraftingTableSpace[id].Count == 1)
                        {
                            count = 1;
                        }
                        while (CraftingTableSpace[id].Count > 0 && count > 0)
                        {
                            HeldItem_block.Push(CraftingTableSpace[id].Pop());
                            count--;
                        }
                    }

                } // if item in selected slot and none held
                else if (CraftingTableSpace[id].Count > 0 && HeldItem_block.Count > 0)
                {
                    if (CraftingTableSpace[id].Peek().type == HeldItem_block.Peek().type && IsItemAllowedInLocation_CraftingTable(id)) // if selected block is from same type
                    {
                        int stackSize = CraftingTableSpace[id].Peek().StackSize; // get the item stack size
                        if (CraftingTableSpace[id].Count < stackSize && HeldItem_block.Count > 0) // add to location till stackSize
                        {
                            CraftingTableSpace[id].Push(HeldItem_block.Pop());
                        }
                    }
                } // if item in slot and held
                else if (CraftingTableSpace[id].Count == 0 && HeldItem_block.Count > 0)
                {
                    if (IsItemAllowedInLocation_CraftingTable(id))
                    {
                        if (HeldItem_block.Count > 0)
                        {
                            CraftingTableSpace[id].Push(HeldItem_block.Pop());
                        }
                    }
                } // if none in spot and held
                print("checking table crafting");
                CheckTableCrafting();
            } // error with crafting recepies when 2 items in slot
            else if (inFurnace)
            {
                if (FurnaceSpaceCounter[id] > 0 && HeldItem_block.Count == 0)
                {
                    if (id == Data.FurnaceOutput)
                    {
                        while (FurnaceSpaceCounter[id] > 0)
                        {
                            HeldItem_block.Push(FurnaceSpace[id]);
                            FurnaceSpaceCounter[id]--;
                        }
                        int slot_index = 0;
                        for (int j = 0; j < 9; j++)
                        {
                            for (int k = 0; k < InventoryData.InvRecipies[CurrentTableRecipeIndex, j, 0] && FurnaceSpaceCounter[slot_index] > 0; k++)
                            {
                                FurnaceSpaceCounter[slot_index]--;
                                if (FurnaceSpaceCounter[slot_index] == 0)
                                {
                                    FurnaceSpace[slot_index] = null;
                                }
                            }
                            slot_index++;
                        }

                    }
                    else
                    {
                        int count = FurnaceSpaceCounter[id] / 2;
                        if (count == 0 && FurnaceSpaceCounter[id] == 1)
                        {
                            count = 1;
                        }
                        while (FurnaceSpaceCounter[id] > 0 && count > 0)
                        {
                            HeldItem_block.Push(FurnaceSpace[id]);
                            FurnaceSpaceCounter[id]--;
                            count--;
                        }
                    }

                } // if item in selected slot and none held
                else if (FurnaceSpaceCounter[id] > 0 && HeldItem_block.Count > 0)
                {
                    if (FurnaceSpace[id].type == HeldItem_block.Peek().type && IsItemAllowedInLocation_Furnace(id, HeldItem_block.Peek())) // if selected block is from same type
                    {
                        int stackSize = FurnaceSpace[id].StackSize; // get the item stack size
                        if (FurnaceSpaceCounter[id] < stackSize && HeldItem_block.Count > 0) // add to location till stackSize
                        {
                            FurnaceSpace[id] = HeldItem_block.Pop();
                            FurnaceSpaceCounter[id]++;
                        }
                    }
                } // if item in slot and held
                else if (FurnaceSpaceCounter[id] == 0 && HeldItem_block.Count > 0)
                {
                    if (IsItemAllowedInLocation_Furnace(id, HeldItem_block.Peek()))
                    {
                        if (HeldItem_block.Count > 0)
                        {
                            FurnaceSpace[id] = HeldItem_block.Pop();
                            FurnaceSpaceCounter[id]++;
                        }
                    }
                } // if none in selected and held

            }
            UpdateGraphics();

            if (HeldItem_block.Count > 0) // if holding something, save location and inBackpack
            {
                HeldItem_isBackpack = inBackpack;
                HeldItem_isBar = !inCraftingTable && !inFurnace && !inBackpack;
                HeldItem_location = id;
            }
        }
    }

    private void SetBarStatus() //set graphics for the bar textures
    {
        for (int i = 0; i < inventoryBarSize; i++) // run on each slot
        {
            if (inventoryBarSpace[i].Count > 0) // if has items
            {
                int j;
                for (j = 0; j < blockTypes.Length; j++) // find texture in blockTypes
                {
                    if (inventoryBarSpace[i].Peek().type == blockTypes[j] && blockTypes[j] != BlockData.BlockTypes.NONE && blockTypes[j] != BlockData.BlockTypes.Tool) // found
                    {
                        inventoryBar[i].texture = Blocks[j]; //set texture and text
                        inventoryBar[i].GetComponentInChildren<Text>().text = (inventoryBarSpace[i].Count > 1) ? inventoryBarSpace[i].Count.ToString() : "";
                        break;
                    }
                }
                if (j == blockTypes.Length) // if not found in blocks
                {
                    for (j = 0; j < partTypes.Length; j++) // find texture in partTypes
                    {
                        if (inventoryBarSpace[i].Peek().partType == partTypes[j]) // found
                        {
                            inventoryBar[i].texture = Parts[j]; //set texture and text
                            inventoryBar[i].GetComponentInChildren<Text>().text = (inventoryBarSpace[i].Count > 1) ? inventoryBarSpace[i].Count.ToString() : "";
                            break;
                        }
                    }
                }
                if (inventoryBarDurabilityViewer[i] && inventoryBarSpace[i].Peek().type == BlockData.BlockTypes.Tool) // if tool, turn on durablity viewer and update slider value
                {
                    inventoryBarDurabilityViewer[i].gameObject.SetActive(true);
                    inventoryBarDurabilityViewer[i].GetComponent<Slider>().value = inventoryBarSpace[i].Peek().toolRankData.Durability / BlockData.ToolRankOffset[inventoryBarSpace[i].Peek().toolRank].Durability;
                }
                else if (inventoryBarDurabilityViewer[i]) // if not tool, turn off 
                {
                    inventoryBarDurabilityViewer[i].gameObject.SetActive(false);
                }
            }
            else // if empty
            {
                inventoryBar[i].texture = Blocks[0]; //transparent texture
                inventoryBar[i].GetComponentInChildren<Text>().text = ""; // clear text
                if (inventoryBarDurabilityViewer[i])
                    inventoryBarDurabilityViewer[i].gameObject.SetActive(false);
            }
        }
    } // update the bar view
    private void SetBackpackStatus() // set graphics for backpack
    {
        for (int i = 0; i < inventoryBackpackSize; i++) // run on each slot
        {
            if (inventoryBackpackSpace[i].Count > 0) // if has items
            {
                int j;
                for (j = 0; j < blockTypes.Length; j++) // run on blockTypes
                {
                    if (inventoryBackpackSpace[i].Peek().type == blockTypes[j] && blockTypes[j] != BlockData.BlockTypes.NONE && blockTypes[j] != BlockData.BlockTypes.Tool) // if found
                    {
                        inventoryBackpack[i].texture = Blocks[j]; // get texture
                        if (inventoryBackpack[i].GetComponentInChildren<Text>()) // set number
                        {
                            inventoryBackpack[i].GetComponentInChildren<Text>().text = (inventoryBackpackSpace[i].Count > 1) ? inventoryBackpackSpace[i].Count.ToString() : "";
                        }
                        break;
                    }
                }
                if (j == blockTypes.Length) // if not found on blocks
                {
                    for (j = 0; j < partTypes.Length; j++) // run on parts
                    {
                        if (inventoryBackpackSpace[i].Peek().partType == partTypes[j]) // if found type
                        {
                            inventoryBackpack[i].texture = Parts[j]; // get texture
                            if (inventoryBackpack[i].GetComponentInChildren<Text>()) // get text
                            {
                                inventoryBackpack[i].GetComponentInChildren<Text>().text = (inventoryBackpackSpace[i].Count > 1) ? inventoryBackpackSpace[i].Count.ToString() : "";
                            }
                            break;
                        }
                    }
                }
                if (inventoryBackpackDurabilityViewer[i] && inventoryBackpackSpace[i].Peek().type == BlockData.BlockTypes.Tool) // if tool, turn on durablity viewer and update slider value
                {
                    inventoryBackpackDurabilityViewer[i].gameObject.SetActive(true);
                    inventoryBackpackDurabilityViewer[i].GetComponent<Slider>().value = inventoryBackpackSpace[i].Peek().toolRankData.Durability / BlockData.ToolRankOffset[inventoryBackpackSpace[i].Peek().toolRank].Durability;
                }
                else if (inventoryBackpackDurabilityViewer[i]) // if not tool, turn off 
                {
                    inventoryBackpackDurabilityViewer[i].gameObject.SetActive(false);
                }
            }
            else // if empty
            {
                inventoryBackpack[i].texture = Blocks[0];//transparent texture
                if (inventoryBackpack[i].GetComponentInChildren<Text>())
                    inventoryBackpack[i].GetComponentInChildren<Text>().text = "";
                if (inventoryBackpackDurabilityViewer[i])
                    inventoryBackpackDurabilityViewer[i].gameObject.SetActive(false);
            }
        }
    }
    private void SetCraftingStatus()// update graphics for crafting table
    {
        for (int i = 0; i < CraftingTableSize; i++) // run on each slot in crafting table
        {
            if (CraftingTableSpace[i].Count > 0) // if has items
            {
                int j;
                for (j = 0; j < blockTypes.Length; j++) // run on blocks
                {
                    if (CraftingTableSpace[i].Peek().type == blockTypes[j] && blockTypes[j] != BlockData.BlockTypes.NONE && blockTypes[j] != BlockData.BlockTypes.Tool) // if found type
                    {
                        CraftingTable[i].texture = Blocks[j]; // get texture
                        if (CraftingTable[i].GetComponentInChildren<Text>()) // get text
                        {
                            CraftingTable[i].GetComponentInChildren<Text>().text = (CraftingTableSpace[i].Count > 1) ? CraftingTableSpace[i].Count.ToString() : "";
                        }
                        break;
                    }
                }
                if (j == blockTypes.Length) // if not found in blocks
                {
                    for (j = 0; j < partTypes.Length; j++) // run on parts
                    {
                        if (CraftingTableSpace[i].Peek().partType == partTypes[j]) // if found type
                        {
                            CraftingTable[i].texture = Parts[j]; // get texture
                            if (CraftingTable[i].GetComponentInChildren<Text>()) // get text
                            {
                                CraftingTable[i].GetComponentInChildren<Text>().text = (CraftingTableSpace[i].Count > 1) ? CraftingTableSpace[i].Count.ToString() : "";
                            }
                            break;
                        }
                    }
                }
                if (CraftingTableDurabilityViewer[i] && CraftingTableSpace[i].Peek().type == BlockData.BlockTypes.Tool) // if tool, turn on durablity viewer and update slider value
                {
                    CraftingTableDurabilityViewer[i].gameObject.SetActive(true);
                    CraftingTableDurabilityViewer[i].GetComponent<Slider>().value = CraftingTableSpace[i].Peek().toolRankData.Durability / BlockData.ToolRankOffset[CraftingTableSpace[i].Peek().toolRank].Durability;
                }
                else if (CraftingTableDurabilityViewer[i]) // if not tool, turn off 
                {
                    CraftingTableDurabilityViewer[i].gameObject.SetActive(false);
                }
            }
            else // if doesnt have any item
            {
                CraftingTable[i].texture = Blocks[0]; // remove texture (blocks[0] is empty texture)
                if (CraftingTable[i].GetComponentInChildren<Text>()) // update text;
                {
                    CraftingTable[i].GetComponentInChildren<Text>().text = "";
                }
                if (CraftingTableDurabilityViewer[i])
                    CraftingTableDurabilityViewer[i].gameObject.SetActive(false);
            }
        }
    }
    public void SetFurnaceStatus()
    {
        for (int i = 0; i < FurnaceSize; i++) // run on each slot in furnace
        {
            if (FurnaceSpaceCounter[i] > 0) // if has items
            {
                int j;
                for (j = 0; j < blockTypes.Length; j++) // run on blocks
                {
                    if (FurnaceSpace[i].type == blockTypes[j] && blockTypes[j] != BlockData.BlockTypes.NONE && blockTypes[j] != BlockData.BlockTypes.Tool) // if found type
                    {
                        Furnace[i].texture = Blocks[j]; // get texture
                        if (Furnace[i].GetComponentInChildren<Text>()) // get text
                        {
                            Furnace[i].GetComponentInChildren<Text>().text = (FurnaceSpaceCounter[i] > 1) ? FurnaceSpaceCounter[i].ToString() : "";
                        }
                        break;
                    }
                }
                if (j == blockTypes.Length) // if not found in blocks
                {
                    for (j = 0; j < partTypes.Length; j++) // run on parts
                    {
                        if (FurnaceSpace[i].partType == partTypes[j]) // if found type
                        {
                            Furnace[i].texture = Parts[j]; // get texture
                            if (Furnace[i].GetComponentInChildren<Text>()) // get text
                            {
                                Furnace[i].GetComponentInChildren<Text>().text = (FurnaceSpaceCounter[i] > 1) ? FurnaceSpaceCounter[i].ToString() : "";
                            }
                            break;
                        }
                    }
                }
                if (FurnaceDurabilityViewer[i] && FurnaceSpace[i].type == BlockData.BlockTypes.Tool) // if tool, turn on durablity viewer and update slider value
                {
                    FurnaceDurabilityViewer[i].gameObject.SetActive(true);
                    FurnaceDurabilityViewer[i].GetComponent<Slider>().value = FurnaceSpace[i].toolRankData.Durability / BlockData.ToolRankOffset[FurnaceSpace[i].toolRank].Durability;
                }
                else if (FurnaceDurabilityViewer[i]) // if not tool, turn off 
                {
                    FurnaceDurabilityViewer[i].gameObject.SetActive(false);
                }
            }
            else // if doesnt have any item
            {
                Furnace[i].texture = Blocks[0]; // remove texture (blocks[0] is empty texture)
                if (Furnace[i].GetComponentInChildren<Text>()) // update text;
                {
                    Furnace[i].GetComponentInChildren<Text>().text = "";
                }
                if (FurnaceDurabilityViewer[i])
                    FurnaceDurabilityViewer[i].gameObject.SetActive(false);
            }
        }
        if (OpenFurnace != null)
        {
            print(OpenFurnace.furnace.worldPosition);
            OpenFurnace.Feul.Clear();
            for (int i = 0; i < FurnaceSpaceCounter[0]; i++)
            {
                OpenFurnace.Feul.Push(FurnaceSpace[0]);
            }
            OpenFurnace.Input.Clear();
            for (int i = 0; i < FurnaceSpaceCounter[1]; i++)
            {
                OpenFurnace.Input.Push(FurnaceSpace[1]);
            }
            OpenFurnace.Output.Clear();
            for (int i = 0; i < FurnaceSpaceCounter[2]; i++)
            {
                OpenFurnace.Output.Push(FurnaceSpace[2]);
            }
        }
    }

    private void SetInvStatus() // call update graphics funcs
    {
        SetBarStatus();
        SetBackpackStatus();
        SetCraftingStatus();
        SetFurnaceStatus();
    }
    public void UpdateGraphics() // call update graphics funcs
    {
        UpdateHeldTexture();
        SetInvStatus();
    }

    public void UpdateHeldTexture() // update the held item texture and number
    {
        int i;
        for (i = 0; i < blockTypes.Length && HeldItem_block.Count > 0; i++) // get the texture for the item
        {
            if (HeldItem_block.Peek().type == blockTypes[i])// found type
            {
                Held_Item.texture = Blocks[i];
                break;
            }
        }
        if (i == blockTypes.Length) // if not found in block
        {
            for (i = 0; i < partTypes.Length && HeldItem_block.Count > 0; i++) // search in part
            {
                if (HeldItem_block.Peek().partType == partTypes[i])// found type
                {
                    Held_Item.texture = Parts[i];
                    break;
                }
            }
        }

        if (HeldItem_block.Count > 0 && HeldItem_block.Peek().type == BlockData.BlockTypes.Tool && HeldItem_block.Peek().toolRankData.Durability < BlockData.ToolRankOffset[HeldItem_block.Peek().toolRank].Durability) // if tool and durability not full
        {
            HeldItem_DurabilitySlider.gameObject.SetActive(true); // setActive durability bar
            float duration = HeldItem_block.Peek().toolRankData.Durability / BlockData.ToolRankOffset[HeldItem_block.Peek().toolRank].Durability;
            HeldItem_DurabilitySlider.value = duration; // set durability bar value
        }
        else // if not tool
        {
            HeldItem_DurabilitySlider.gameObject.SetActive(false); // disable durability bar
        }

        if (HeldItem_block.Count > 1) // get the count text
        {
            Held_Item.GetComponentInChildren<Text>().text = HeldItem_block.Count.ToString();
        }
        else
        {
            Held_Item.GetComponentInChildren<Text>().text = "";
        }
    }

    public bool DropItemFunc(Vector3 position, Item block, float counter_value = -1, int count = 1) // drop block in position with timer of counter_value and at count of count
    {
        GameObject dropItem = Instantiate(DropItem, position, Quaternion.identity); // create the gameobject

        Collider[] playerCol = MainScript.player.GetComponentsInChildren<Collider>(); // get all the colliders from the childs of player
        Collider cubeCollider = dropItem.GetComponent<Collider>();
        Collider shpereCollider = dropItem.GetComponentInChildren<Collider>();
        foreach (Collider collider in playerCol)// run on them
        {
            if (collider.gameObject.tag != "IgnoreColliderTag") // if th colliser doesnt have a IgnoreCollisderTag (the pickup system), it sets it to ignore it
            {
                Physics.IgnoreCollision(cubeCollider, collider);
                Physics.IgnoreCollision(collider, cubeCollider);
                Physics.IgnoreCollision(shpereCollider, collider);
                Physics.IgnoreCollision(collider, shpereCollider);
            }
        }
        Physics.IgnoreCollision(cubeCollider, MainScript.player.GetComponent<Collider>()); // ignore the main collider of the player (not in child)
        Physics.IgnoreCollision(MainScript.player.GetComponent<Collider>(), cubeCollider);
        Physics.IgnoreCollision(shpereCollider, MainScript.player.GetComponent<Collider>());
        Physics.IgnoreCollision(MainScript.player.GetComponent<Collider>(), shpereCollider);


        dropItem.transform.SetParent(GameObject.FindGameObjectWithTag("DroppedItems").transform); // set parent
        DropedItem temp_script = dropItem.GetComponent<DropedItem>(); // get script
        temp_script.myType = new DroppedItemData(block, count); // set texture and block

        Material temp_mat = null;

        if (block.type != BlockData.BlockTypes.NONE && block.type != BlockData.BlockTypes.Tool) // if block
        {
            for (int i = 0; i < blockTypes.Length; i++) // check in block types
            {
                if (blockTypes[i] == temp_script.myType.Item.Peek().type && blockTypes[i] != BlockData.BlockTypes.NONE && blockTypes[i] != BlockData.BlockTypes.Tool) // if found type
                {
                    temp_mat = droppedBlock[i]; // get mat at location
                    break;
                }
            }
        }
        else // if part
        {
            for (int i = 0; i < partTypes.Length; i++) // check in part types
            {
                if (partTypes[i] == temp_script.myType.Item.Peek().partType) // if found type
                {
                    temp_mat = droppedPart[i]; // get mat at location
                    break;
                }
            }
        }

        temp_script.render.material = temp_mat; //temp_mat; // apply mat

        if (counter_value != -1) // if counter isnt default,
        {
            temp_script.counter = counter_value; // set counter value
        }
        print(temp_script.myType.Count);
        return true;
    } // drop block at position
    public bool DropItemFunc(Vector3 position, byte ArrLocation, float counter_value = -1, int count = 1) // drop block in position with timer of counter_value and at count of count
    {
        GameObject dropItem = Instantiate(DropItem, position, Quaternion.identity); // create the gameobject

        Collider[] playerCol = MainScript.player.GetComponentsInChildren<Collider>(); // get all the colliders from the childs of player
        Collider cubeCollider = dropItem.GetComponent<Collider>();
        Collider shpereCollider = dropItem.GetComponentInChildren<Collider>();
        foreach (Collider collider in playerCol)// run on them
        {
            if (collider.gameObject.tag != "IgnoreColliderTag") // if th colliser doesnt have a IgnoreCollisderTag (the pickup system), it sets it to ignore it
            {
                Physics.IgnoreCollision(cubeCollider, collider);
                Physics.IgnoreCollision(collider, cubeCollider);
                Physics.IgnoreCollision(shpereCollider, collider);
                Physics.IgnoreCollision(collider, shpereCollider);
            }
        }
        Physics.IgnoreCollision(cubeCollider, MainScript.player.GetComponent<Collider>()); // ignore the main collider of the player (not in child)
        Physics.IgnoreCollision(MainScript.player.GetComponent<Collider>(), cubeCollider);
        Physics.IgnoreCollision(shpereCollider, MainScript.player.GetComponent<Collider>());
        Physics.IgnoreCollision(MainScript.player.GetComponent<Collider>(), shpereCollider);


        dropItem.transform.SetParent(GameObject.FindGameObjectWithTag("DroppedItems").transform); // set parent
        DropedItem temp_script = dropItem.GetComponent<DropedItem>(); // get script
        temp_script.myType = new DroppedItemData(ArrLocation, count); // set texture and block

        Material temp_mat = null;

        if (BlockData.ItemTypesArr[ArrLocation].type != BlockData.BlockTypes.NONE && BlockData.ItemTypesArr[ArrLocation].type != BlockData.BlockTypes.Tool) // if block
        {
            for (int i = 0; i < blockTypes.Length; i++) // check in block types
            {
                if (blockTypes[i] == temp_script.myType.Item.Peek().type && blockTypes[i] != BlockData.BlockTypes.NONE && blockTypes[i] != BlockData.BlockTypes.Tool) // if found type
                {
                    temp_mat = droppedBlock[i]; // get mat at location
                    break;
                }
            }
        }
        else // if part
        {
            for (int i = 0; i < partTypes.Length; i++) // check in part types
            {
                if (partTypes[i] == temp_script.myType.Item.Peek().partType) // if found type
                {
                    temp_mat = droppedPart[i]; // get mat at location
                    break;
                }
            }
        }

        temp_script.render.material = temp_mat; //temp_mat; // apply mat

        if (counter_value != -1) // if counter isnt default,
        {
            temp_script.counter = counter_value; // set counter value
        }
        print(temp_script.myType.Count);
        return true;
    } // drop block at position

    bool IsItemAllowedInLocationBackpack(Item item, int location) // only in normal inventory // if allowed in inventory
    {
        //print((location != Data.inventoryCraftingOutput) + ", " + !(location <= Data.inventoryArmorHelmetVal && location >= Data.inventoryArmorBootsVal) + ", " + (item.armorType == BlockData.ArmorType.Boots && location == Data.inventoryArmorBootsVal) + ", " + (item.armorType == BlockData.ArmorType.Leggings && location == Data.inventoryArmorLeggingsVal) + ", " + (item.armorType == BlockData.ArmorType.Chestpiece && location == Data.inventoryArmorChestpieceVal) + ", " + (item.armorType == BlockData.ArmorType.Helmet && location == Data.inventoryArmorHelmetVal) + ", " + location + ", " + item.armorType.ToString());
        return (location != Data.inventoryCraftingOutput) && (!(location <= Data.inventoryArmorHelmetVal && location >= Data.inventoryArmorBootsVal) || (item.armorType == BlockData.ArmorType.Boots && location == Data.inventoryArmorBootsVal) || (item.armorType == BlockData.ArmorType.Leggings && location == Data.inventoryArmorLeggingsVal) || (item.armorType == BlockData.ArmorType.Chestpiece && location == Data.inventoryArmorChestpieceVal) || (item.armorType == BlockData.ArmorType.Helmet && location == Data.inventoryArmorHelmetVal));
    }
    bool InInvCraftingArea(int location) // if location in inventory crafting area
    {
        return Util.BetweenInc(location, inventoryBackpackSize - 1, inventoryBackpackSize - 5);
    }

    bool IsItemAllowedInLocation_CraftingTable(int location) // if location allowed in crafting table
    {
        return location != Data.CraftingTableOutput;
    }
    bool IsItemAllowedInLocation_Furnace(int location, Item block) // if location allowed in furnace
    {
        return location != Data.FurnaceOutput && ((block.isBurnable && location == 0) || (block.isCookable && location == 1)); // if not in output, and (is a fuel and in foul slot) or (is cookable and in cook slot)
    }

    void CheckInvCrafting() // checks crafting for inventory crafting area (check CheckTableCrafting() for details)
    {
        int slot_index = Data.inventoryCraftingTableBottomVal;
        bool recipie_correct = true;
        for (int i = 0; i < InventoryData.InvRecipieCount; i++)
        {
            slot_index = 31;
            recipie_correct = true;
            for (int j = 0; j < 4; j++)
            {
                if (!(inventoryBackpackSpace[slot_index].Count >= InventoryData.InvRecipies[i, j, 0] && ((inventoryBackpackSpace[slot_index].Count == 0) ? true : (inventoryBackpackSpace[slot_index].Peek().ArrLocation == InventoryData.InvRecipies[i, j, 1]))))
                {
                    recipie_correct = false;
                    //print(recipie_correct + ", " + (inventoryBackpackSpace[slot_index].Count > 0) + ", " + ((inventoryBackpackSpace[slot_index].Count > 0) && inventoryBackpackSpace[slot_index].Count >= InventoryData.InvRecipies[i, j, 0]) + ", " + ((inventoryBackpackSpace[slot_index].Count > 0) && inventoryBackpackSpace[slot_index].Peek().ArrLocation == InventoryData.InvRecipies[i, j, 1]));
                }
                //print(recipie_correct + ", " + ((inventoryBackpackSpace[slot_index].Count > 0) && inventoryBackpackSpace[slot_index].Count > 0) + ", " + ((inventoryBackpackSpace[slot_index].Count > 0) && inventoryBackpackSpace[slot_index].Count >= InventoryData.recipies[i, j, 0]) + ", " + ((inventoryBackpackSpace[slot_index].Count > 0) && inventoryBackpackSpace[slot_index].Peek().ArrLocation == InventoryData.recipies[i, j, 1]));
                slot_index++;
            }
            print(recipie_correct);
            if (recipie_correct)
            {
                while (inventoryBackpackSpace[inventoryBackpackSize - 1].Count > 0)
                {
                    inventoryBackpackSpace[inventoryBackpackSize - 1].Pop();
                }
                for (int j = 0; j < InventoryData.InvRecipies[i, 4, 0]; j++)
                {
                    inventoryBackpackSpace[inventoryBackpackSize - 1].Push(Util.GetNewItem(InventoryData.InvRecipies[i, 4, 1]));
                }
                CurrentInvCraftingRecipeIndex = i;
                UpdateGraphics();
                return;
            }
            while (inventoryBackpackSpace[inventoryBackpackSize - 1].Count > 0)
            {
                inventoryBackpackSpace[inventoryBackpackSize - 1].Pop();
            }
        }
        CurrentInvCraftingRecipeIndex = -1;

    }

    void CheckTableCrafting() // check recipies for crafting table
    {

        int slot_index = 0;
        bool recipie_correct = true;
        for (int i = 0; i < InventoryData.TableRecipieCount; i++) // run on recipies
        {
            slot_index = 0;
            recipie_correct = true;
            for (int j = 0; j < 9; j++) // run on slots in recipie and crafting table
            {
                if (!(CraftingTableSpace[slot_index].Count >= InventoryData.TableRecipies[i, j, 0] && ((CraftingTableSpace[slot_index].Count == 0) ? true : (CraftingTableSpace[slot_index].Peek().ArrLocation == InventoryData.TableRecipies[i, j, 1])))) // if recipie for [i,j] incorrect
                {
                    recipie_correct = false; // recipie not correct
                    //print("fell on: " + i + ", " + j + ", bools: " + (CraftingTableSpace[slot_index].Count >= InventoryData.TableRecipies[i, j, 0]) + ", " + ((CraftingTableSpace[slot_index].Count == 0) ? true : (CraftingTableSpace[slot_index].Peek().ArrLocation == InventoryData.TableRecipies[i, j, 1])));
                }
                slot_index++;
            }
            //print(recipie_correct + ", " + i);
            if (recipie_correct) // if correct
            {
                CraftingTableSpace[CraftingTableSize - 1].Clear();
                for (int j = 0; j < InventoryData.TableRecipies[i, 9, 0]; j++) // add to output the recipie output
                {
                    Item temp = Util.GetNewItem(InventoryData.TableRecipies[i, 9, 1]);
                    print("crafted item: " + temp + ", arrLoc: " + InventoryData.TableRecipies[i, 9, 1] + ", arrLocOriginalBlock: " + BlockData.ItemTypesArr[InventoryData.TableRecipies[i, 9, 1]]);
                    CraftingTableSpace[CraftingTableSize - 1].Push(temp);
                }
                CurrentTableRecipeIndex = i; // save recipie index
                UpdateGraphics(); // update the graphics
                return; // exit
            }
            CraftingTableSpace[CraftingTableSize - 1].Clear();
        }
        //print("clear output");
        CurrentTableRecipeIndex = -1; // set craftingIndex to none
        UpdateGraphics(); // update graphics
    }

    public void SetGui(int id, bool status) // sets gui[id] to status
    {
        switch (id)
        {
            case -1:
                break;
            case 0: // crafting table
                BlockGUIs[0].SetActive(status);
                isCraftingOpen = status;
                isBackpackOpen = status;
                break;
            case 1:
                BlockGUIs[1].SetActive(status);
                isBackpackOpen = status;
                break;
            default:
                break;
        }
    }
    public void CloseAllGUIs() // run on all guis and close them
    {
        foreach (GameObject gui in BlockGUIs) // run on guis
        {
            gui.SetActive(false); // close
        }
        if (OpenGuiBlock != null)
            OpenGuiBlock.SetGui(false);
    }

    public void ClearCraftingTable() // if closes crafting table and items in it, drop all on player
    {
        for (int idx = 0; idx < Data.CraftingTableOutput - 1; idx++)// run on crafting table slots
        {
            while (CraftingTableSpace[idx].Count > 0) // while slot not empty
            {
                DropItemFunc(MainScript.playerController.gameObject.transform.position, CraftingTableSpace[idx].Peek(), 0);     //CraftingTableSpace[idx]
                CraftingTableSpace[idx].Pop(); // drop it on player, and remove from stack
            }
        }
        CraftingTableSpace[Data.CraftingTableOutput].Clear();
    }

    public Item HeldTool()// return held item
    {
        return (inventoryBarSpace[Location].Count > 0) ? inventoryBarSpace[Location].Peek() : null;
    }

    public bool IsHeldItemABlock() // if item in bar[location] is a block
    {
        return inventoryBarSpace[Location].Count > 0 && inventoryBarSpace[Location].Peek().type != BlockData.BlockTypes.NONE && inventoryBarSpace[Location].Peek().type != BlockData.BlockTypes.Tool;
    }

    public void UpdateDurabilityOfHeldTool()
    {
        if (inventoryBarSpace[Location].Count > 0 && inventoryBarSpace[Location].Peek().type == BlockData.BlockTypes.Tool)
        {
            inventoryBarSpace[Location].Peek().toolRankData.Durability--;
            float duration = inventoryBarSpace[Location].Peek().toolRankData.Durability / BlockData.ToolRankOffset[inventoryBarSpace[Location].Peek().toolRank].Durability;
            inventoryBarDurabilityViewer[Location].value = duration;
            if (inventoryBarSpace[Location].Peek().toolRankData.Durability == -1)
            {
                inventoryBarSpace[Location].Pop();
                UpdateGraphics();
            }
        }
    }

    public void RefrehHeldBlock()
    {
        if(inventoryBarSpace[Location].Count > 0 && inventoryBarSpace[Location].Peek().Scripted)
        {
            Item temp = inventoryBarSpace[Location].Pop();
            inventoryBarSpace[Location].Push(Util.GetNewItem(temp.ArrLocation));
        }
    }
}