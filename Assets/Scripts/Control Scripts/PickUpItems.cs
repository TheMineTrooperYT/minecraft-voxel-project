﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpItems : MonoBehaviour {

    public Player MainScript;

	// Use this for initialization
	void Start () {
        if (MainScript == null)
            MainScript = GameObject.FindGameObjectWithTag("MainScript").GetComponent<Player>();
        //print("started pickUpItems;");
	}

    void OnTriggerStay(Collider collider)
    {
        if (collider.tag == "DroppedItem")
        {
            try
            {
                DroppedItemData data = collider.gameObject.GetComponent<DropedItem>().tryPick();
                if (data != null && !MainScript.Dead)
                {
                    bool fail = false;
                    int i;
                    while (data.Item.Count > 0)
                    {
                        if (!MainScript.inventory.addBlock(data.Item.Pop(), true))
                        {
                            fail = true;
                            break;
                        }
                    }
                    if (fail)
                    {
                        MainScript.inventory.DropItemFunc(collider.gameObject.transform.position,data.Item.Peek(),count:(data.Item.Count));
                    }
                    else
                    {
                        Destroy(collider.gameObject);
                    }
                }
            }
            catch (System.NullReferenceException) { }
        }
    }
}
