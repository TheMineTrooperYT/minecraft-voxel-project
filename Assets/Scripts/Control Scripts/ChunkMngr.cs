﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class ChunkData
{
    public static readonly int ChunkCountX = 20;
    public static readonly int ChunkCountZ = 20;

    public static readonly int ChunkRenderDistance = 150; // in blocks // the render distance
    public static readonly float ChunkRenderRefreshTimer = 5; // 5s // the time between each render check
}

public class ChunkMngr : MonoBehaviour
{
    public Player MainScript;

    public GameObject Chunk;

    public List<GameObject> Chunks;

    public GameObject LoadingScreen;
    public bool loadingScrrenOpen;
    // Use this for initialization
    void Start()
    {
        toggleLoadingScreen(); // start loading screen
        StartCoroutine(CreateMap()); // start CreateMap Coroutine
    }

    public IEnumerator CreateMap()
    {
        VoxelData.seed = Random.Range(100000,999999); // get a new seed
        Vector3 pos = this.transform.position; // get this pos
        Vector3 offset = Vector3.zero; // set offset
        MainScript.SpawnPoint = new Vector3(ChunkData.ChunkCountX / 2 * VoxelData.ChunkWidth + 0.5f, Data.PlayerHeight, ChunkData.ChunkCountZ / 2 * VoxelData.ChunkWidth + 0.5f);
        MainScript.player.transform.position = MainScript.SpawnPoint; // move player to middle of map (y will set later)
        int x = 0,z = 0;
        Chunk chunk = null;
        for (x = 0; x < ChunkData.ChunkCountX; x++) // run on chunk count X
        {
            offset.z = 0; // reset offset.z
            if (x % 2 == 0) // yield every two passes
            {
                yield return null;
            }

            for (z = 0; z < ChunkData.ChunkCountZ; z++) // run on the chunk count Z
            {
                GameObject temp =  createChunk(pos + offset); // get the created chunk
                if (z == ChunkData.ChunkCountZ/2 && x == ChunkData.ChunkCountX/2) // if in middle of map
                {
                    temp.GetComponent<Chunk>().getHeight = true; // set this chunk to get height for player
                }
                temp.GetComponent<MeshRenderer>().enabled = false; // disable the renderer
                offset += Vector3.forward * VoxelData.ChunkWidth; // advance the offset
                chunk = temp.GetComponent<Chunk>();
                chunk.Last = false;
            }
            offset += Vector3.right * VoxelData.ChunkWidth; // advance offset
        }
        chunk.Last = true;
        //print("chunk last: " + chunk.gameObject.transform.position);
        yield return null; // yield
    }

    GameObject createChunk(Vector3 worldPosition) // genereate a new chunk at worldPosition
    {
        GameObject chunk = Instantiate(Chunk, worldPosition, Quaternion.identity); // initiate object
        chunk.transform.SetParent(GameObject.FindGameObjectWithTag("ChunkMngr").transform); // set the parent
        Chunks.Add(chunk); // add to list
        return chunk; // return chunk
    }

    public Chunk GetChunkAtPos(Vector3 position) // get chunk the position is in its bounds
    {
        GameObject[] arr = Chunks.ToArray(); // get array of chunks
        
        for (int i = 0; i < arr.Length; i++) // run on chunks
        {
            float dist = arr[i].GetComponent<Chunk>().GetDistance(position); // get the distance
            
            if (dist == -1) // if distance return -1 (in chunk) 
            {
                return arr[i].GetComponent<Chunk>(); // return this chunk
            }
        }

        return null; // if none found, return null
    }

    public void CallTreeGen()
    {
        Chunks.ForEach(chunk =>
        {
            chunk.GetComponent<Chunk>().GenerateTrees();
        });
        StartCoroutine(FinishLoading());
    }

    public IEnumerator FinishLoading()
    {
        print("finish loading");
        Chunks.ForEach(chunk =>
        {
            chunk.GetComponent<Chunk>().UpdateMesh();
        });
        toggleLoadingScreen();
        yield return null;
    }

    public void toggleLoadingScreen() // toggle the lloading screen on/of
    {
        loadingScrrenOpen = (loadingScrrenOpen) ? false : true; // bool
        LoadingScreen.SetActive((loadingScrrenOpen) ? true : false); // gameObject
        MainScript.playerController.chunkRenderer.GetChunkDisances();
        print("toggle loading screen ------------------------------------------ " + loadingScrrenOpen);
    }
}
