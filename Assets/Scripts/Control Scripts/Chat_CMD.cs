﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chat_CMD : MonoBehaviour
{

    public Player MainScript;

    public InputField inputField;
    public GameObject inpGameObj;

    public Text OutPutText;
    public GameObject outObj;

    public readonly int MaxLineCount = 29;

    public float Timer_def = 15f;
    public float timer = 0; // set to -15 to turn off
                            // Use this for initialization
    void Start()
    {
        if (inputField == null)
            inputField = GetComponentInChildren<InputField>();
        if (OutPutText == null)
            OutPutText = GetComponentInChildren<Text>();
        if (MainScript == null)
            MainScript = GameObject.FindGameObjectWithTag("MainScript").GetComponent<Player>();
        print("created chat");
    }

    // Update is called once per frame
    void Update()
    {
        if (timer > 0)
            timer -= Time.deltaTime;
        if (timer <= 0 && timer != -15 && outObj.activeSelf)
        {
            outObj.SetActive(false);
        }
    }

    public void ParseCommand()
    {
        string text = this.inputField.text; // get the command string
        inputField.text = "";
        inpGameObj.SetActive(false); // close the input

        string cmd;
        string[] msg = new string[1];
        if (text.StartsWith("/")) // command
        {
            string output = text + "\n"; // try get output
            text = text.Substring(1); // remove /
            if (text.Length == 0)
            {
                addToTextView("Unknown command.");
                timer = Timer_def; // start dissaper timer
                return;
            }
            try
            {
                if (text.IndexOf(' ') == -1) // if no spaces
                {
                    cmd = text; // text
                    msg = new string[0]; // msg.Length = 0
                }
                else
                {
                    cmd = text.Substring(0, text.IndexOf(' ')); // get the first word
                    text = text.Substring(text.IndexOf(' ') + 1); // get the rest
                    if (text.IndexOf(' ') != -1)// there are different params
                    {
                        msg = text.Split(' '); // split to msg
                    }
                    else
                    {
                        msg[0] = text; // put all to [0]
                    }
                }
            }
            catch (System.Exception e)
            {
                print(e.ToString()); // error
                throw;
            }


            switch (cmd) // all th different commands
            {
                case "give": // give command. /give <'block'/'item'> <Block/Item type> [<count (number)>]
                    if (msg.Length < 1) // if not enough parameters
                    {
                        output = "Invalid Format! format:(/give <'block'/'item'> <Block/Item type> [<count (number)>])";
                    }
                    else
                    {
                        bool error = false;

                        Item item = null;

                        switch (msg[0]) // switch on the block type
                        {
                            case "grass":
                                item = new Block_Grass(null, Vector3.zero); // grass
                                break;
                            case "dirt":
                                item = new Block_Dirt(null, Vector3.zero); // dirt
                                break;
                            case "stone":
                                item = new Block_Stone(null, Vector3.zero); // stone
                                break;
                            case "bedrock":
                                item = new Block_Bedrock(null, Vector3.zero); // bedrock
                                break;
                            case "cobblestone":
                                item = new Block_Cobblestone(null, Vector3.zero); // bedrock
                                break;
                            case "glass_none":
                                item = new Block_Glass_none(null, Vector3.zero); // normal glass
                                break;
                            case "brick_wall":
                                item = new Block_Brick_Wall(null, Vector3.zero); // brick_wall
                                break;
                            case "crafting_table":
                                item = new Block_Crafting_Table(null, Vector3.zero); // crafting table
                                break;
                            case "planks_oak":
                                item = new Block_Planks_Oak(null, Vector3.zero); // crafting table
                                break;
                            case "snowy_grass":
                                item = new Block_Snowy_Grass(null, Vector3.zero); // snowy grass
                                break;
                            case "sea_lantern":
                                item = new Block_Sea_Lantern(null, Vector3.zero);
                                break;
                            case "log_oak":
                                item = new Block_Log_Oak(null, Vector3.zero);
                                break;
                            case "furnace":
                                item = new Block_Furnace(null, Vector3.zero);
                                break;


                            // tools
                            case "pickaxe_iron":
                                item = new Tool_Pickaxe_Iron();
                                break;
                            case "axe_iron":
                                item = new Tool_Axe_Iron();
                                break;
                            case "pickaxe_stone":
                                item = new Tool_Pickaxe_Stone();
                                break;
                            case "axe_stone":
                                item = new Tool_Axe_Stone();
                                break;



                            // parts
                            case "stick":
                                item = new Part_Stick();
                                break;


                            default:
                                output += "Invalid command! [" + msg[0] + "] is not a block type!"; // none of the above
                                error = true;
                                break;
                        }

                        if (!error) // if didnt fail above
                        {
                            bool err1 = false;
                            int size;
                            if (msg.Length == 2) // if has a size param
                            {
                                if (!int.TryParse(msg[1], out size)) // try parse it to size
                                {
                                    output += "Invalid command! [" + msg[2] + "] is not a number!"; // failed
                                    err1 = true;
                                }
                                /*if (!err1 && size > item.StackSize) // if success and ober the item stack size, shrink to stack size
                                {
                                    size = item.StackSize;
                                }*/
                            }
                            else // if no size param
                            {
                                size = 1;
                            }

                            if (!err1)// if didnt fail above
                            {
                                MainScript.inventory.DropItemFunc(MainScript.player.transform.position, item.ArrLocation, 0, size); // add to inventory size items or untill failed to add to inv

                                output += "Given " + size.ToString() + " " + item.type.ToString() + ((size == 1) ? " block" : " blokcs");
                            }
                        }
                    }
                    break;
                case "tp": // teleport command. /tp <x> <y> <z> (~ for current value) or /tp spawnpoint
                    if (msg.Length < 3 || msg.Length >= 4)
                    {
                        if (msg.Length == 1 && msg[0] == "spawnpoint") // if /tp spawnpoint teleport to spawnpoint
                        {
                            MainScript.playerController.gameObject.transform.position = MainScript.SpawnPoint;
                            output += "Teleported player to " + MainScript.SpawnPoint + " (spawnpoint)";
                        }
                        else
                        {
                            output += "Invalid format! format:(/tp <x> <y> <z> (~ for current value) or /tp spawnpoint)";
                        }
                    }
                    else
                    {
                        bool error = false;
                        float x = 0, y = 0, z = 0;
                        if (msg[0] == "~")
                            x = MainScript.playerController.transform.position.x;
                        else if (!error)
                        {
                            if (!float.TryParse(msg[0], out x))
                            {
                                output += "Invalid command! [" + msg[0] + "] isn't a float!";
                                error = true;
                            }
                        }
                        if (msg[1] == "~")
                            y = MainScript.playerController.transform.position.y;
                        else if (!error)
                        {
                            if (!float.TryParse(msg[1], out y))
                            {
                                output += "Invalid command! [" + msg[1] + "] isn't a float!";
                                error = true;
                            }
                        }
                        if (msg[2] == "~")
                            z = MainScript.playerController.transform.position.z;
                        else if (!error)
                        {
                            if (!float.TryParse(msg[2], out z))
                            {
                                output += "Invalid command! [" + msg[2] + "] isn't a float!";
                                error = true;
                            }
                        }

                        if (!error)
                        {
                            print(x + ", " + y + ", " + z);
                            MainScript.playerController.transform.position = new Vector3(x, y, z);
                            output += "Teleported player to " + new Vector3(x, y, z);
                        }
                    }
                    break;
                case "spawnpoint": // set spawnpoint command /spawnpoint
                    if (msg.Length > 0)
                    {
                        output += "Invalid format! /spawnpoint doesn't take any arguments";
                    }
                    else
                    {
                        MainScript.SpawnPoint = MainScript.playerController.gameObject.transform.position + (Vector3.up * 1.5f);
                        output += "Set spawnpoint to " + MainScript.SpawnPoint;
                    }
                    break;
                case "kill": // kill the player. all the msg is the reason for death. /kill [<reason>]
                    string reason = "";
                    foreach (string item in msg)
                    {
                        reason += item + " ";
                    }
                    MainScript.Kill(reason);
                    output += Data.PlayerName + ((reason == "") ? " fell out of the world" : (" died by " + reason));
                    break;
                case "gamemode": // gamemode command. /gamemode <mode> (modes: 1:creative,0:survival)
                    if (msg.Length > 1)
                    {
                        output += "Invalid format! format:(/gamemode [<mode>] (modes: 1:creative,0:survival))";
                    }
                    else
                    {
                        if (msg.Length != 0)
                        {
                            int mode;
                            if (!int.TryParse(msg[0], out mode))
                            {
                                output += "Invalid command! [" + msg[0] + "] isn't a mode!";
                            }
                            else
                            {
                                bool error = true;

                                foreach (int gameMode in Data.GameModes)
                                {
                                    if (mode == gameMode)
                                    {
                                        error = false;
                                        break;
                                    }
                                }

                                if (!error)
                                {
                                    Data.GameMode_int = mode;
                                    output += "Changed gamemode to " + mode.ToString();
                                }
                                else
                                {
                                    output += "Invalid command! [" + msg[0] + "] isn't and available mode!";
                                }
                            }
                        }
                        else
                        {
                            string mode = "";
                            switch (Data.GameMode_int)
                            {
                                case 0:
                                    mode = "survival";
                                    break;
                                case 1:
                                    mode = "creative";
                                    break;
                                default:
                                    break;
                            }
                            output += Data.PlayerName + " is in " + mode + " gamemode";
                        }
                    }
                    break;
                case "gamerule": // gamerule command. changes a game rule. /gamerule <rule> [<true/false>]
                    if (msg.Length == 0 || msg.Length > 2)
                    {
                        output += "Invalid format! format:(/gamerule <rule> [<true/false>])";
                    }
                    else
                    {
                        string rule = msg[0];
                        if (msg.Length == 2)
                        {
                            bool value;

                            if (!bool.TryParse(msg[1], out value))
                            {
                                output += "Invalid command! [" + msg[1] + "] isn't a boolean!";
                            }
                            else
                            {
                                switch (rule)
                                {
                                    case "KeepInventory":
                                        Data.KeepInventory = value;
                                        break;
                                    case "CommandBlockOutput":
                                        Data.CommendBlockOutput = value;
                                        break;
                                    case "DoubleSpaceToFly":
                                        Data.DoubleSpaceToFly = value;
                                        break;
                                    default:
                                        output += "Invalid command! [" + rule + "] isn't a gamerule!";
                                        break;
                                }
                            }
                        }
                        else
                        {
                            switch (rule)
                            {
                                case "KeepInventory":
                                    output += Data.KeepInventory;
                                    break;
                                case "CommandBlockOutput":
                                    output += Data.CommendBlockOutput;
                                    break;
                                case "DoubleSpaceToFly":
                                    output += Data.DoubleSpaceToFly;
                                    break;
                                default:
                                    output += "Invalid command! [" + rule + "] isn't a gamerule!";
                                    break;
                            }
                        }
                    }
                    break;
                case "fly": // fly command. changes flight. /fly [true/false] (if none, toggles)
                    if (msg.Length > 1)
                    {
                        output += "Invalid format! format:(/fly [true/false])";
                    }
                    else
                    {
                        if (msg.Length == 1)
                        {
                            bool value;

                            if (!bool.TryParse(msg[0], out value))
                            {
                                output += "Invalid command! [" + msg[0] + "] isn't a boolean!";
                            }
                            else
                            {
                                MainScript.playerController.ToggleFlight(value);
                            }
                        }
                        else
                        {
                            MainScript.playerController.ToggleFlight();
                        }
                    }
                    break;
                case "clear": // clear command. clears the inventory. /clear
                    if (msg.Length > 0)
                    {
                        output += "Invalid foramt! format:(/clear)";
                    }
                    else
                    {
                        for (int i = 0; i < MainScript.inventory.inventoryBackpackSize; i++)
                        {
                            while (MainScript.inventory.inventoryBackpackSpace[i].Count > 0)
                            {
                                MainScript.inventory.inventoryBackpackSpace[i].Pop();
                            }
                        }

                        for (int i = 0; i < MainScript.inventory.inventoryBarSize; i++)
                        {
                            while (MainScript.inventory.inventoryBarSpace[i].Count > 0)
                            {
                                MainScript.inventory.inventoryBarSpace[i].Pop();
                            }
                        }
                        MainScript.inventory.UpdateGraphics();
                        output += "Cleared Inventory";
                    }
                    break;
                case "hurt": // hurt command. hurts player. /hurt <count> [<reason>] // reson will be displayed if player died from hurt
                    if (msg.Length < 1)
                    {
                        output += "Invalid Format! format:(/hurt <count> [<reason>])";
                    }
                    else
                    {
                        int count;
                        if (!int.TryParse(msg[0],out count))
                        {
                            output += "Invalid parameters! ["+msg[0]+"] is not a number";
                        }
                        else
                        {
                            MainScript.Hurt(count, (msg.Length == 2) ? msg[1] : "punching himself in the face");
                            output += "Hurted " + Data.PlayerName + " by " + (count/2f).ToString() + " hearts";
                        }
                    }
                    break;
                case "heal": // heal command. heals player. /heal <count>
                    if (msg.Length < 1)
                    {
                        output += "Invalid Format! format:(/heal <count>)";
                    }
                    else
                    {
                        int count = 0;
                        if (!int.TryParse(msg[0],out count))
                        {
                            output += "Invalid Parameters! ["+msg[0]+"] is not a number!";
                        }
                        else
                        {
                            MainScript.Heal(count);
                            output += "Healed "+Data.PlayerName+" for " + (count/2f).ToString() + " hearts";
                        }
                    }
                    break;
                default: // none of the above
                    output += "Unknown command.";
                    break;
            }
            addToTextView(output);
        }
        else // normal msg
        {
            addToTextView("[" + Data.PlayerName + "]: " + text);
        }
        timer = Timer_def; // start disapear timer
    }

    void addToTextView(string text) // adds text to Text text
    {
        int length;
        string temp = "";
        string[] temp_arr;
        if (OutPutText.text.Contains("\n")) // id ther are \n's
        {
            temp_arr = OutPutText.text.Split('\n');// split and get length
            length = temp_arr.Length;
        }
        else
        {
            length = 1; // if there are none, length = 1
        }
        if (length == MaxLineCount) // if max
        {
            temp = this.OutPutText.text.Substring(this.OutPutText.text.IndexOf('\n') + 1); // then remove the last line
        }
        else
        {
            temp = OutPutText.text; // if not max, stay same
        }
        if (!temp.EndsWith("\n")) // if doesnt end with \n, add \n at end
            temp += "\n";

        temp += text; // add the new line
        temp_arr = temp.Split('\n'); // ------------------- if with the new line its larger than max line numer, then remove lines until its good
        while (temp_arr.Length >= MaxLineCount + 1)
        {
            temp = temp.Substring(temp.IndexOf('\n') + 1);
            temp_arr = temp.Split('\n');
        } // ----------------------------------------------

        this.OutPutText.text = temp; // set text
    }


}