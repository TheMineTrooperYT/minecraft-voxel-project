﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundDetector : MonoBehaviour {

    public Player MainScript;

    public bool ExecuteCode = false;

    public bool IsGrounded = false;
    public Vector3 LastPos = Vector3.zero;
    

    void Start()
    {
        LastPos = MainScript.player.transform.position;
    }

    void OnTriggerEnter(Collider collider)
    {
        IsGrounded = true;

        if (ExecuteCode)
        {
            float dif = LastPos.y - gameObject.transform.parent.transform.position.y;
            if (dif > 6)
            {
                float normalized = (dif / (Data.FallDamageKillHeight - Data.minimumFallDamageHeight)) * 20;
                //print(dif + ", " + normalized + ", " + LastPos + ", " + transform.position + ", " + (dif / (Data.FallDamageKillHeight - Data.minimumFallDamageHeight)));
                MainScript.Hurt((int)normalized, "Falling From High Place");
            }
        }

        LastPos = MainScript.player.transform.position;
    }

    void OnTriggerStay(Collider collider)
    {
        if (!IsGrounded)
            IsGrounded = true;
    }

    void OnTriggerExit(Collider collider)
    {
        IsGrounded = false;
    }
}
