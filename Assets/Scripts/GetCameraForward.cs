﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetCameraForward : MonoBehaviour {

    public Camera myCam;

    public Vector3 GetCamForward()
    {
        return transform.TransformDirection(Vector3.forward);
    }
}
