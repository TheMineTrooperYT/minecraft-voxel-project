﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropedItem : MonoBehaviour
{

    public float DeathCounter = 300;

    public float counter = 1.5f;
    public DroppedItemData myType;

    public Renderer render;
    public Rigidbody RB;

    int LayerMask;
    // Use this for initialization
    void Start()
    {
        if (render == null) // initialize renderer
            render = GetComponent<Renderer>();
        if (RB == null) // initialize rigidbody
            RB = GetComponent<Rigidbody>();
        LayerMask = 1 << 8; // set layermask    
        LayerMask = ~LayerMask;
    }

    // Update is called once per frame
    void Update()
    {
        if (counter > 0) // drop counter
            counter -= Time.deltaTime; // reduce
        if (DeathCounter > 0) // death counter
            DeathCounter -= Time.deltaTime; // reduce
        if (DeathCounter <= 0) // if death counter  <= 0
            Destroy(gameObject); // kill this item (happens after 5 minutes)

        if (transform.position.y < -10) // if fell out of map
        {
            Destroy(gameObject); // kill
        }

        Vector3 Point; // ------------------------------------------------------------ check wether the item is inside a collider (block placed over it)
        Vector3 Start = new Vector3(transform.position.x, -5, transform.position.z);
        Vector3 Goal = transform.position;
        Vector3 Direction = Goal - Start;
        Direction.Normalize();
        int Itterations = 0; // If we know how many times the raycast has hit faces on its way to the target and back, we can tell through logic whether or not it is inside.
        Point = Start;

        while (Point != Goal) // Try to reach the point starting from the far off point.  This will pass through faces to reach its objective.
        {
            RaycastHit hit;
            if (Physics.Linecast(Point, Goal, out hit, LayerMask)) // Progressively move the point forward, stopping everytime we see a new plane in the way.
            {
                if (hit.transform.gameObject.tag != "Player" && hit.transform.gameObject.tag != "IgnoreColliderTag" && hit.transform.gameObject.tag != "DroppedItemChild")
                {
                    Itterations++;
                    Point = hit.point + (Direction / 100.0f); // Move the Point to hit.point and push it forward just a touch to move it through the skin of the mesh (if you don't push it, it will read that same point indefinately).
                }
            }
            else
            {
                Point = Goal; // If there is no obstruction to our goal, then we can reach it in one step.
            }
        }
        while (Point != Start) // Try to return to where we came from, this will make sure we see all the back faces too.
        {
            RaycastHit hit;
            if (Physics.Linecast(Point, Start, out hit, LayerMask))
            {
                if (hit.transform.gameObject.tag != "Player" && hit.transform.gameObject.tag != "IgnoreColliderTag" && hit.transform.gameObject.tag != "DroppedItemChild")
                {
                    Itterations++;
                    Point = hit.point + (-Direction / 100.0f);
                }
            }
            else
            {
                Point = Start;
            }
        }
        if (Itterations % 2 == 1)
        {
            RB.velocity = Vector3.zero; // if in, velocity = zero, 
            transform.Translate(0f, 0.5f, 0f); // up
        } // ---------------------------------------------- end /\/\/\/\/\/\/\/\/\/\

    }

    public DroppedItemData tryPick() // try to pick the item
    {
        if (counter <= 0) // if pickup counter finished
        {
            return myType; // return saved item
        }
        return null; // pickup counter not finished, returns null
    }
}

public class DroppedItemData
{
    public Stack<Item> Item { get; }
    public int Count { get; private set; }

    public DroppedItemData(Item item, int count)
    {
        Item = new Stack<Item>();
        for (int i = 0; i < count; i++)
            Item.Push(item);
    }
    public DroppedItemData(byte ArrLocation, int count)
    {
        Item = new Stack<Item>();
        for (int i = 0; i < count; i++)
        {
            Item.Push(Util.GetNewItem(ArrLocation));
        }
    }

    public void AddItems(Item item, int count)
    {
        if ((Item.Count > 0 && item.type == Item.Peek().type) || Item.Count == 0)
            for (int i = 0; i < count; i++)
                Item.Push(item);
    }
    public void AddItems(byte ArrLocation, int count)
    {
        if ((Item.Count > 0 && ArrLocation == Item.Peek().ArrLocation) || Item.Count == 0)
            for (int i = 0; i < count; i++)
                Item.Push(Util.GetNewItem(ArrLocation));
    }

    public void RemoveItems(Item item, int count)
    {
        if (Item.Count > 0 && item.type == Item.Peek().type)
            for (int i = 0; i < count; i++)
                Item.Pop();
    }

    public Item GetItem()
    {
        if (Item.Count == 0)
        {
            return null;
        }
        return Item.Pop();
    }
}