﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ToolRankData
{
    private float _MineTime; // in seconds
    private float _Damage; // in health value (non exsistent yet)

    public ToolRankData(float mine, float durability, float damage)
    {
        _MineTime = mine;
        Durability = durability;
        _Damage = damage;
    }

    public float MineTime
    {
        get { return _MineTime; }
        set
        {
            if (value > 0)
            {
                _MineTime = value;
            }
        }
    }

    public float Durability { get; set; }

    public float Damage
    {

        get
        {
            return _Damage;
        }
        set
        {
            if (value > 0)
            {
                _Damage = value;
            }
        }
    }
}

public static class BlockData
{
    public static readonly float TextureOffset = 0.03125f; // block size in the texture file

    public enum ArmorType
    {
        NONE,
        Helmet,
        Chestpiece,
        Leggings,
        Boots
    } // armor types

    public enum ToolAbilityTypes
    {
        Melee, // sword, others
        Range, // bow, others
        Both, // Melee And Mining // axe, pickaxe, shovel, etc.

        NONE,
    }

    public enum PartTypes
    {
        Wood_Shovel,
        Wood_Hoe,
        Wood_Pickaxe,
        Wood_Axe,
        Wood_Sword,

        Stone_Shovel,
        Stone_Hoe,
        Stone_Pickaxe,
        Stone_Axe,
        Stone_Sword,

        Iron_Shovel,
        Iron_Hoe,
        Iron_Pickaxe,
        Iron_Axe,
        Iron_Sword,

        Gold_Shovel,
        Gold_Hoe,
        Gold_Pickaxe,
        Gold_Axe,
        Gold_Sword,

        Diamond_Shovel,
        Diamond_Hoe,
        Diamond_Pickaxe,
        Diamond_Axe,
        Diamond_Sword,
        // tools /\/\/\/\

        Stick,
        NONE,
    }


    public enum ToolRank
    {
        Wood,
        Stone,
        Iron,
        Gold,
        Diamond,

        NONE
    } // tool ranks
    public static readonly ToolRankData ToolRankDefaultValues = new ToolRankData(1, 1, 1);
    public static readonly float HandMineSpeed = 3f * ToolRankDefaultValues.MineTime;
    public static readonly Dictionary<ToolRank, ToolRankData> ToolRankOffset = new Dictionary<ToolRank, ToolRankData>()
    {
        { ToolRank.Wood , new ToolRankData(2 * ToolRankDefaultValues.MineTime, 59 * ToolRankDefaultValues.Durability, 0) }, // (mine time, durability uses)
        { ToolRank.Stone , new ToolRankData(0.5f * ToolRankDefaultValues.MineTime, 131 * ToolRankDefaultValues.Durability, 0)  },
        { ToolRank.Iron , new ToolRankData(0.33f * ToolRankDefaultValues.MineTime, 250 * ToolRankDefaultValues.Durability, 0)  }, // change dur' to 250
        { ToolRank.Gold , new ToolRankData(0.25f * ToolRankDefaultValues.MineTime, 32 * ToolRankDefaultValues.Durability, 0)  },
        { ToolRank.Diamond , new ToolRankData(0.25f * ToolRankDefaultValues.MineTime, 1561 * ToolRankDefaultValues.Durability, 0)  },
        { ToolRank.NONE, new ToolRankData(HandMineSpeed,-1,0) },
    };


    public enum ArmorRank
    {
        Leather,
        Iron,
        Gold,
        Diamond
    } // armor ranks

    public enum BlockTypes
    {
        Air,
        Grass,
        Dirt,
        Stone,
        Bedrock,
        Glass_none,
        Cobblestone,
        BrickWall,
        CraftingTable,
        Planks_Oak,
        Snowy_Grass,
        Sea_Lantern,
        Log_Oak,
        Leafes_Def,
        Furnace,

        NONE, // doesnt include those in the array
        Tool,
    } // block types
    public static Item[] ItemTypesArr = new Item[] { // 1:grass,2:dirt,3:stone,4:bedrock
        new Empty(), // 0
        new Block_Grass(null,Vector3.zero), //1
        new Block_Dirt(null,Vector3.zero),//2
        new Block_Stone(null,Vector3.zero),//3
        new Block_Bedrock(null,Vector3.zero),//4
        new Block_Glass_none(null,Vector3.zero),//5
        new Block_Cobblestone(null,Vector3.zero),//6
        new Block_Brick_Wall(null,Vector3.zero),//7
        new Block_Crafting_Table(null,Vector3.zero),//8
        new Block_Planks_Oak(null,Vector3.zero),//9
        new Block_Snowy_Grass(null,Vector3.zero),//10
        new Block_Sea_Lantern(null,Vector3.zero),//11
        new Block_Log_Oak(null,Vector3.zero), // 12 // Y
        new Block_Log_Oak(null,Vector3.zero), // 13 // Z
        new Block_Log_Oak(null,Vector3.zero), // 14 // X
        new Block_Leafs_Def(null,Vector3.zero), // 15
        new Tool_Pickaxe_Iron(), // 16
        new Tool_Axe_Iron(), // 17
        new Part_Stick(), // 18
        new Tool_Pickaxe_Stone(), // 19
        new Tool_Axe_Stone(), // 20
        new Block_Furnace(null, Vector3.zero), // 21 // +x
        new Block_Furnace(null, Vector3.zero), // 22 // -x
        new Block_Furnace(null, Vector3.zero), // 23 // +z
        new Block_Furnace(null, Vector3.zero), // 24 // -z
    }; // item types as array

    public static readonly int Log_Directions_Loaction_bot = 12;
    public static readonly int Log_Directions_Loaction_top = 14;

    /*
     
      { //         (horizontal, vertical)
		    new Vector2 ( 0 , 0 ), // bottom right
		    new Vector2 ( 0 , 0 ), // top right  
            new Vector2 ( 0 , 0 ), // bottom left
		    new Vector2 ( 0 , 0 ), // top left
      },
      // check order: left-right-top-bottom-back-forward
      */
    public static readonly Vector2[,,] voxelUvs = new Vector2[,,]{
        { // vert: 11 (to bottom); horz: 15 (to left)
            //          (horizontal, vertical) // order: bottom_let,bottom_right,top_left,top_right 
            {
                new Vector2 ( TextureOffset*13,1-(TextureOffset * 11)), // bottom right
                new Vector2 ( TextureOffset*13,1-(TextureOffset*10)),
                new Vector2 ((TextureOffset * 12),1-(TextureOffset * 11)), // bottom left
		        new Vector2 ( TextureOffset*12,1-(TextureOffset*10)) // top left
            }, // top right  
        
            {
                new Vector2 ( TextureOffset*13,1-(TextureOffset * 11)), // bottom right
                new Vector2 ( TextureOffset*13,1-(TextureOffset*10)),
                new Vector2 ((TextureOffset * 12),1-(TextureOffset * 11)), // bottom left
		        new Vector2 ( TextureOffset*12,1-(TextureOffset*10)) // top left
            }, // top right  
        
            {
                new Vector2 ((TextureOffset * 15),1-(TextureOffset * 11)), // bottom left
		        new Vector2 ( TextureOffset*16,1-(TextureOffset * 11)), // bottom right
		        new Vector2 ( TextureOffset*15,1-(TextureOffset*10)), // top left
		        new Vector2 ( TextureOffset*16,1-(TextureOffset*10))
            }, // top right   
        
            {
                new Vector2 ((TextureOffset * 8),1-(TextureOffset * 6)), // bottom left // 8,7(bottom)
		        new Vector2 ( TextureOffset*9,1-(TextureOffset * 6)), // bottom right
		        new Vector2 ( TextureOffset*8,1-(TextureOffset*7)), // top left
		        new Vector2 ( TextureOffset*9,1-(TextureOffset*7))
            }, // top right
        
            {
                new Vector2 ( TextureOffset*13,1-(TextureOffset * 11)), // bottom right
                new Vector2 ( TextureOffset*13,1-(TextureOffset*10)),
                new Vector2 ((TextureOffset * 12),1-(TextureOffset * 11)), // bottom left
		        new Vector2 ( TextureOffset*12,1-(TextureOffset*10)) // top left
            }, // top right  
        
            {
                new Vector2 ( TextureOffset*13,1-(TextureOffset * 11)), // bottom right
                new Vector2 ( TextureOffset*13,1-(TextureOffset*10)),
                new Vector2 ((TextureOffset * 12),1-(TextureOffset * 11)), // bottom left
		        new Vector2 ( TextureOffset*12,1-(TextureOffset*10)) // top left
            }, // top right  

        }, // grass
        { // vert: 11 (to bottom); horz: 15 (to left)// check order: left-right-top-bottom-back-forward
            //          (horizontal, vertical) // order: bottom_let,bottom_right,top_left,top_right 
            {
                new Vector2 ((TextureOffset * 8),1-(TextureOffset * 6)), // bottom left // 8,7(bottom)
		        new Vector2 ( TextureOffset*9,1-(TextureOffset * 6)), // bottom right
		        new Vector2 ( TextureOffset*8,1-(TextureOffset*7)), // top left
		        new Vector2 ( TextureOffset*9,1-(TextureOffset*7))
            }, // top right
                
            {
                new Vector2 ((TextureOffset * 8),1-(TextureOffset * 6)), // bottom left // 8,7(bottom)
		        new Vector2 ( TextureOffset*9,1-(TextureOffset * 6)), // bottom right
		        new Vector2 ( TextureOffset*8,1-(TextureOffset*7)), // top left
		        new Vector2 ( TextureOffset*9,1-(TextureOffset*7))
            }, // top right

            {
                new Vector2 ((TextureOffset * 8),1-(TextureOffset * 6)), // bottom left // 8,7(bottom)
		        new Vector2 ( TextureOffset*9,1-(TextureOffset * 6)), // bottom right
		        new Vector2 ( TextureOffset*8,1-(TextureOffset*7)), // top left
		        new Vector2 ( TextureOffset*9,1-(TextureOffset*7))
            }, // top right

            {
                new Vector2 ((TextureOffset * 8),1-(TextureOffset * 6)), // bottom left // 8,7(bottom)
		        new Vector2 ( TextureOffset*9,1-(TextureOffset * 6)), // bottom right
		        new Vector2 ( TextureOffset*8,1-(TextureOffset*7)), // top left
		        new Vector2 ( TextureOffset*9,1-(TextureOffset*7))
            }, // top right

            {
                new Vector2 ((TextureOffset * 8),1-(TextureOffset * 6)), // bottom left // 8,7(bottom)
		        new Vector2 ( TextureOffset*9,1-(TextureOffset * 6)), // bottom right
		        new Vector2 ( TextureOffset*8,1-(TextureOffset*7)), // top left
		        new Vector2 ( TextureOffset*9,1-(TextureOffset*7))
            }, // top right

            {
                new Vector2 ((TextureOffset * 8),1-(TextureOffset * 6)), // bottom left // 8,7(bottom)
		        new Vector2 ( TextureOffset*9,1-(TextureOffset * 6)), // bottom right
		        new Vector2 ( TextureOffset*8,1-(TextureOffset*7)), // top left
		        new Vector2 ( TextureOffset*9,1-(TextureOffset*7))
            }, // top right

        }, // dirt
        { // vert: -10 (to bottom); horz: -11 (to left) // check order: left-right-top-bottom-back-forward
            //          (horizontal, vertical) // BL-TL,TL-TR,TR-BR,BR-BL
            {
                new Vector2 ( 1-(TextureOffset*12),1-(TextureOffset * 10)), // bottom right
		        new Vector2 ( 1-(TextureOffset*12),1-(TextureOffset*9)), // top right
                new Vector2 (1-(TextureOffset * 11),1-(TextureOffset * 10)), // bottom left
		        new Vector2 ( 1-(TextureOffset*11),1-(TextureOffset*9)), // top left
            }, // top right  

            {
                new Vector2 ( 1-(TextureOffset*12),1-(TextureOffset * 10)), // bottom right
		        new Vector2 ( 1-(TextureOffset*12),1-(TextureOffset*9)),
                new Vector2 (1-(TextureOffset * 11),1-(TextureOffset * 10)), // bottom left
		        new Vector2 ( 1-(TextureOffset*11),1-(TextureOffset*9)), // top left
            }, // top right  

            {
                new Vector2 ( 1-(TextureOffset*12),1-(TextureOffset * 10)), // bottom right
		        new Vector2 ( 1-(TextureOffset*12),1-(TextureOffset*9)),
                new Vector2 (1-(TextureOffset * 11),1-(TextureOffset * 10)), // bottom left
		        new Vector2 ( 1-(TextureOffset*11),1-(TextureOffset*9)), // top left
            }, // top right   

            {
                new Vector2 ( 1-(TextureOffset*12),1-(TextureOffset * 10)), // bottom right
		        new Vector2 ( 1-(TextureOffset*12),1-(TextureOffset*9)),
                new Vector2 (1-(TextureOffset * 11),1-(TextureOffset * 10)), // bottom left
		        new Vector2 ( 1-(TextureOffset*11),1-(TextureOffset*9)), // top left
            }, // top right  

            {
                new Vector2 ( 1-(TextureOffset*12),1-(TextureOffset * 10)), // bottom right
		        new Vector2 ( 1-(TextureOffset*12),1-(TextureOffset*9)),
                new Vector2 (1-(TextureOffset * 11),1-(TextureOffset * 10)), // bottom left
		        new Vector2 ( 1-(TextureOffset*11),1-(TextureOffset*9)), // top left
            }, // top right   

            {
                new Vector2 ( 1-(TextureOffset*12),1-(TextureOffset * 10)), // bottom right
		        new Vector2 ( 1-(TextureOffset*12),1-(TextureOffset*9)),
                new Vector2 (1-(TextureOffset * 11),1-(TextureOffset * 10)), // bottom left
		        new Vector2 ( 1-(TextureOffset*11),1-(TextureOffset*9)), // top left
            }, // top right  

        }, // stone
        {
             // -4 to bottom; +4 to left
            {
                new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 4) ), // bottom right
		        new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 3) ),
                new Vector2 ( ( TextureOffset * 4 ), 1 - ( TextureOffset * 4 ) ), // bottom left
		        new Vector2 ( ( TextureOffset * 4 ), 1 - ( TextureOffset * 3 ) ), // top left
            }, // top right  
            {
                new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 4) ), // bottom right
		        new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 3) ),
                new Vector2 ( ( TextureOffset * 4 ), 1 - ( TextureOffset * 4 ) ), // bottom left
		        new Vector2 ( ( TextureOffset * 4 ), 1 - ( TextureOffset * 3 ) ), // top left
            }, // top right   
            {
                new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 4) ), // bottom right
		        new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 3) ),
                new Vector2 ( ( TextureOffset * 4 ), 1 - ( TextureOffset * 4 ) ), // bottom left
		        new Vector2 ( ( TextureOffset * 4 ), 1 - ( TextureOffset * 3 ) ), // top left
            }, // top right  
            {
                new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 4) ), // bottom right
		        new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 3) ),
                new Vector2 ( ( TextureOffset * 4 ), 1 - ( TextureOffset * 4 ) ), // bottom left
		        new Vector2 ( ( TextureOffset * 4 ), 1 - ( TextureOffset * 3 ) ), // top left
            }, // top right  
            {
                new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 4) ), // bottom right
		        new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 3) ),
                new Vector2 ( ( TextureOffset * 4 ), 1 - ( TextureOffset * 4 ) ), // bottom left
		        new Vector2 ( ( TextureOffset * 4 ), 1 - ( TextureOffset * 3 ) ), // top left
            }, // top right  
            {
                new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 4) ), // bottom right
		        new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 3) ),
                new Vector2 ( ( TextureOffset * 4 ), 1 - ( TextureOffset * 4 ) ), // bottom left
		        new Vector2 ( ( TextureOffset * 4 ), 1 - ( TextureOffset * 3 ) ), // top left
            }, // top right  

        }, // bedrock
        {
             // -9 to bottom; +7 to left
            {
                new Vector2 ( ( TextureOffset * 8 ), 1 - ( TextureOffset * 9 ) ), // bottom right
		        new Vector2 ( ( TextureOffset * 8 ), 1 - ( TextureOffset * 8 ) ),
                new Vector2 ( ( TextureOffset * 7 ), 1 - ( TextureOffset * 9 ) ), // bottom left
		        new Vector2 ( ( TextureOffset * 7 ), 1 - ( TextureOffset * 8 ) ), // top left
            }, // top right  
            {
                new Vector2 ( ( TextureOffset * 8 ), 1 - ( TextureOffset * 9 ) ), // bottom right
		        new Vector2 ( ( TextureOffset * 8 ), 1 - ( TextureOffset * 8 ) ),
                new Vector2 ( ( TextureOffset * 7 ), 1 - ( TextureOffset * 9 ) ), // bottom left
		        new Vector2 ( ( TextureOffset * 7 ), 1 - ( TextureOffset * 8 ) ), // top left
            }, // top right  
            {
                new Vector2 ( ( TextureOffset * 8 ), 1 - ( TextureOffset * 9 ) ), // bottom right
		        new Vector2 ( ( TextureOffset * 8 ), 1 - ( TextureOffset * 8 ) ),
                new Vector2 ( ( TextureOffset * 7 ), 1 - ( TextureOffset * 9 ) ), // bottom left
		        new Vector2 ( ( TextureOffset * 7 ), 1 - ( TextureOffset * 8 ) ), // top left
            }, // top right  
            {
                new Vector2 ( ( TextureOffset * 8 ), 1 - ( TextureOffset * 9 ) ), // bottom right
		        new Vector2 ( ( TextureOffset * 8 ), 1 - ( TextureOffset * 8 ) ),
                new Vector2 ( ( TextureOffset * 7 ), 1 - ( TextureOffset * 9 ) ), // bottom left
		        new Vector2 ( ( TextureOffset * 7 ), 1 - ( TextureOffset * 8 ) ), // top left
            }, // top right  
            {
                new Vector2 ( ( TextureOffset * 8 ), 1 - ( TextureOffset * 9 ) ), // bottom right
		        new Vector2 ( ( TextureOffset * 8 ), 1 - ( TextureOffset * 8 ) ),
                new Vector2 ( ( TextureOffset * 7 ), 1 - ( TextureOffset * 9 ) ), // bottom left
		        new Vector2 ( ( TextureOffset * 7 ), 1 - ( TextureOffset * 8 ) ), // top left
            }, // top right  
            {
                new Vector2 ( ( TextureOffset * 8 ), 1 - ( TextureOffset * 9 ) ), // bottom right
		        new Vector2 ( ( TextureOffset * 8 ), 1 - ( TextureOffset * 8 ) ),
                new Vector2 ( ( TextureOffset * 7 ), 1 - ( TextureOffset * 9 ) ), // bottom left
		        new Vector2 ( ( TextureOffset * 7 ), 1 - ( TextureOffset * 8 ) ), // top left
            }, // top right  
        
        }, // glass_none
        { // vert: -6 (to bottom); horz: 3 (to left)// check order: left-right-top-bottom-back-forward
            //          (horizontal, vertical) // order: bottom_let,bottom_right,top_left,top_right 
            {
                new Vector2 ( TextureOffset*4,1-(TextureOffset * 6)), // bottom right
                new Vector2 ( TextureOffset*4,1-(TextureOffset*5)),
                new Vector2 ((TextureOffset * 3),1-(TextureOffset * 6)), // bottom left
		        new Vector2 ( TextureOffset*3,1-(TextureOffset*5)) // top left
            }, // top right  
        
            {
                new Vector2 ( TextureOffset*4,1-(TextureOffset * 6)), // bottom right
                new Vector2 ( TextureOffset*4,1-(TextureOffset*5)),
                new Vector2 ((TextureOffset * 3),1-(TextureOffset * 6)), // bottom left
		        new Vector2 ( TextureOffset*3,1-(TextureOffset*5)) // top left
            }, // top right  
        
            {
                new Vector2 ( TextureOffset*4,1-(TextureOffset * 6)), // bottom right
                new Vector2 ( TextureOffset*4,1-(TextureOffset*5)),
                new Vector2 ((TextureOffset * 3),1-(TextureOffset * 6)), // bottom left
		        new Vector2 ( TextureOffset*3,1-(TextureOffset*5)) // top left
            }, // top right  
        
            {
                new Vector2 ( TextureOffset*4,1-(TextureOffset * 6)), // bottom right
                new Vector2 ( TextureOffset*4,1-(TextureOffset*5)),
                new Vector2 ((TextureOffset * 3),1-(TextureOffset * 6)), // bottom left
		        new Vector2 ( TextureOffset*3,1-(TextureOffset*5)) // top left
            }, // top right  
        
            {
                new Vector2 ( TextureOffset*4,1-(TextureOffset * 6)), // bottom right
                new Vector2 ( TextureOffset*4,1-(TextureOffset*5)),
                new Vector2 ((TextureOffset * 3),1-(TextureOffset * 6)), // bottom left
		        new Vector2 ( TextureOffset*3,1-(TextureOffset*5)) // top left
            }, // top right  
        
            {
                new Vector2 ( TextureOffset*4,1-(TextureOffset * 6)), // bottom right
                new Vector2 ( TextureOffset*4,1-(TextureOffset*5)),
                new Vector2 ((TextureOffset * 3),1-(TextureOffset * 6)), // bottom left
		        new Vector2 ( TextureOffset*3,1-(TextureOffset*5)) // top left
            }, // top right  

        }, // cobblestone
        {
             // -4 to bottom; +4 to left
            {
                new Vector2 ( ( TextureOffset * 6 ), 1 - ( TextureOffset * 4) ), // bottom right
		        new Vector2 ( ( TextureOffset * 6 ), 1 - ( TextureOffset * 3) ),
                new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 4 ) ), // bottom left
		        new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 3 ) ), // top left
            }, // top right  
            {
                new Vector2 ( ( TextureOffset * 6 ), 1 - ( TextureOffset * 4) ), // bottom right
		        new Vector2 ( ( TextureOffset * 6 ), 1 - ( TextureOffset * 3) ),
                new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 4 ) ), // bottom left
		        new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 3 ) ), // top left
            }, // top right   
            {
                new Vector2 ( ( TextureOffset * 6 ), 1 - ( TextureOffset * 4) ), // bottom right
		        new Vector2 ( ( TextureOffset * 6 ), 1 - ( TextureOffset * 3) ),
                new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 4 ) ), // bottom left
		        new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 3 ) ), // top left
            }, // top right  
            {
                new Vector2 ( ( TextureOffset * 6 ), 1 - ( TextureOffset * 4) ), // bottom right
		        new Vector2 ( ( TextureOffset * 6 ), 1 - ( TextureOffset * 3) ),
                new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 4 ) ), // bottom left
		        new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 3 ) ), // top left
            }, // top right  
            {
                new Vector2 ( ( TextureOffset * 6 ), 1 - ( TextureOffset * 4) ), // bottom right
		        new Vector2 ( ( TextureOffset * 6 ), 1 - ( TextureOffset * 3) ),
                new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 4 ) ), // bottom left
		        new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 3 ) ), // top left
            }, // top right  
            {
                new Vector2 ( ( TextureOffset * 6 ), 1 - ( TextureOffset * 4) ), // bottom right
		        new Vector2 ( ( TextureOffset * 6 ), 1 - ( TextureOffset * 3) ),
                new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 4 ) ), // bottom left
		        new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 3 ) ), // top left
            }, // top right  

        }, // brick_wall
        {

            {
                new Vector2 ( ( TextureOffset * 4 ), 1 - ( TextureOffset * 7) ), // bottom right // -7 to bottom; +3 to left
		        new Vector2 ( ( TextureOffset * 4 ), 1 - ( TextureOffset * 6) ),
                new Vector2 ( ( TextureOffset * 3 ), 1 - ( TextureOffset * 7 ) ), // bottom left
		        new Vector2 ( ( TextureOffset * 3 ), 1 - ( TextureOffset * 6 ) ), // top left
            }, // top right  
            {
                new Vector2 ( ( TextureOffset * 4 ), 1 - ( TextureOffset * 7) ), // bottom right // -7 to bottom; +3 to left
		        new Vector2 ( ( TextureOffset * 4 ), 1 - ( TextureOffset * 6) ),
                new Vector2 ( ( TextureOffset * 3 ), 1 - ( TextureOffset * 7 ) ), // bottom left
		        new Vector2 ( ( TextureOffset * 3 ), 1 - ( TextureOffset * 6 ) ), // top left
            }, // top right   
            {
                new Vector2 ( ( TextureOffset * 6 ), 1 - ( TextureOffset * 7) ), // bottom right // top // -7 to bottom; +5 to left
		        new Vector2 ( ( TextureOffset * 6 ), 1 - ( TextureOffset * 6) ),
                new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 7 ) ), // bottom left
		        new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 6 ) ), // top left
            }, // top right  
            {
                new Vector2 ( ( TextureOffset * 16 ), 1 - ( TextureOffset * 5) ), // bottom right // bottom (wooden planks normal) 
		        new Vector2 ( ( TextureOffset * 16 ), 1 - ( TextureOffset * 4) ),
                new Vector2 ( ( TextureOffset * 17 ), 1 - ( TextureOffset * 5 ) ), // bottom left // 17 to left, -5 to bottom
		        new Vector2 ( ( TextureOffset * 17 ), 1 - ( TextureOffset * 4 ) ), // top left
            }, // top right  
            {
                new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 7 ) ), // bottom right// -7 to bottom; +4 to left
		        new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 6 ) ),
                new Vector2 ( ( TextureOffset * 4 ), 1 - ( TextureOffset * 7 ) ), // bottom left
		        new Vector2 ( ( TextureOffset * 4 ), 1 - ( TextureOffset * 6 ) ), // top left
            }, // top right  
            {
                new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 7) ), // bottom right// -7 to bottom; +4 to left
		        new Vector2 ( ( TextureOffset * 5 ), 1 - ( TextureOffset * 6) ),
                new Vector2 ( ( TextureOffset * 4 ), 1 - ( TextureOffset * 7 ) ), // bottom left
		        new Vector2 ( ( TextureOffset * 4 ), 1 - ( TextureOffset * 6 ) ), // top left
            }, // top right  

        }, // crafting_table
        {
            {
                new Vector2 ( ( TextureOffset * 16 ), 1 - ( TextureOffset * 5) ), // bottom right // bottom (wooden planks normal) 
		        new Vector2 ( ( TextureOffset * 16 ), 1 - ( TextureOffset * 4) ),
                new Vector2 ( ( TextureOffset * 17 ), 1 - ( TextureOffset * 5 ) ), // bottom left // 17 to left, -5 to bottom
		        new Vector2 ( ( TextureOffset * 17 ), 1 - ( TextureOffset * 4 ) ), // top left
            }, // top right  
               {
                new Vector2 ( ( TextureOffset * 16 ), 1 - ( TextureOffset * 5) ), // bottom right // bottom (wooden planks normal) 
		        new Vector2 ( ( TextureOffset * 16 ), 1 - ( TextureOffset * 4) ),
                new Vector2 ( ( TextureOffset * 17 ), 1 - ( TextureOffset * 5 ) ), // bottom left // 17 to left, -5 to bottom
		        new Vector2 ( ( TextureOffset * 17 ), 1 - ( TextureOffset * 4 ) ), // top left
            }, // top right  
            {
                new Vector2 ( ( TextureOffset * 16 ), 1 - ( TextureOffset * 5) ), // bottom right // bottom (wooden planks normal) 
		        new Vector2 ( ( TextureOffset * 16 ), 1 - ( TextureOffset * 4) ),
                new Vector2 ( ( TextureOffset * 17 ), 1 - ( TextureOffset * 5 ) ), // bottom left // 17 to left, -5 to bottom
		        new Vector2 ( ( TextureOffset * 17 ), 1 - ( TextureOffset * 4 ) ), // top left
            }, // top right  
            {
                new Vector2 ( ( TextureOffset * 16 ), 1 - ( TextureOffset * 5) ), // bottom right // bottom (wooden planks normal) 
		        new Vector2 ( ( TextureOffset * 16 ), 1 - ( TextureOffset * 4) ),
                new Vector2 ( ( TextureOffset * 17 ), 1 - ( TextureOffset * 5 ) ), // bottom left // 17 to left, -5 to bottom
		        new Vector2 ( ( TextureOffset * 17 ), 1 - ( TextureOffset * 4 ) ), // top left
            }, // top right  
            {
                new Vector2 ( ( TextureOffset * 16 ), 1 - ( TextureOffset * 5) ), // bottom right // bottom (wooden planks normal) 
		        new Vector2 ( ( TextureOffset * 16 ), 1 - ( TextureOffset * 4) ),
                new Vector2 ( ( TextureOffset * 17 ), 1 - ( TextureOffset * 5 ) ), // bottom left // 17 to left, -5 to bottom
		        new Vector2 ( ( TextureOffset * 17 ), 1 - ( TextureOffset * 4 ) ), // top left
            }, // top right  
            {
                new Vector2 ( ( TextureOffset * 16 ), 1 - ( TextureOffset * 5) ), // bottom right // bottom (wooden planks normal) 
		        new Vector2 ( ( TextureOffset * 16 ), 1 - ( TextureOffset * 4) ),
                new Vector2 ( ( TextureOffset * 17 ), 1 - ( TextureOffset * 5 ) ), // bottom left // 17 to left, -5 to bottom
		        new Vector2 ( ( TextureOffset * 17 ), 1 - ( TextureOffset * 4 ) ), // top left
            }, // top right  
        }, // planks_oak
        {

           { //         (horizontal, vertical) // +14 to left, -11 to bottom
		        new Vector2 ( TextureOffset*15 , 1 - (TextureOffset * 11) ), // bottom right
		        new Vector2 ( TextureOffset*15 , 1 - (TextureOffset * 10) ), // top right  
                new Vector2 ( TextureOffset*14 , 1 - (TextureOffset * 11) ), // bottom left
		        new Vector2 ( TextureOffset*14 , 1 - (TextureOffset * 10) ), // top left
           },
           { //         (horizontal, vertical) // +14 to left, -11 to bottom
		        new Vector2 ( TextureOffset*15 , 1 - (TextureOffset * 11) ), // bottom right
		        new Vector2 ( TextureOffset*15 , 1 - (TextureOffset * 10) ), // top right  
                new Vector2 ( TextureOffset*14 , 1 - (TextureOffset * 11) ), // bottom left
		        new Vector2 ( TextureOffset*14 , 1 - (TextureOffset * 10) ), // top left
           },
           { //         (horizontal, vertical) // +0 to left, -20 to bottom
		        new Vector2 ( TextureOffset , 1 - (TextureOffset * 20) ), // bottom right
		        new Vector2 ( TextureOffset , 1 - (TextureOffset * 19) ), // top right  
                new Vector2 ( 0 , 1 - (TextureOffset * 20) ), // bottom left
		        new Vector2 ( 0 , 1 - (TextureOffset * 19) ), // top left
           },
            {
                new Vector2 ((TextureOffset * 8),1-(TextureOffset * 6)), // bottom left // 8,7(bottom)
		        new Vector2 ( TextureOffset*9,1-(TextureOffset * 6)), // bottom right
		        new Vector2 ( TextureOffset*8,1-(TextureOffset*7)), // top left
		        new Vector2 ( TextureOffset*9,1-(TextureOffset*7))
            }, // top right
           { //         (horizontal, vertical) // +14 to left, -11 to bottom
		        new Vector2 ( TextureOffset*15 , 1 - (TextureOffset * 11) ), // bottom right
		        new Vector2 ( TextureOffset*15 , 1 - (TextureOffset * 10) ), // top right  
                new Vector2 ( TextureOffset*14 , 1 - (TextureOffset * 11) ), // bottom left
		        new Vector2 ( TextureOffset*14 , 1 - (TextureOffset * 10) ), // top left
           },
           { //         (horizontal, vertical) // +14 to left, -11 to bottom
		        new Vector2 ( TextureOffset*15 , 1 - (TextureOffset * 11) ), // bottom right
		        new Vector2 ( TextureOffset*15 , 1 - (TextureOffset * 10) ), // top right  
                new Vector2 ( TextureOffset*14 , 1 - (TextureOffset * 11) ), // bottom left
		        new Vector2 ( TextureOffset*14 , 1 - (TextureOffset * 10) ), // top left
           },

        }, // snowy_grass
        { // -12 to left, -4 bottom 
            {
                new Vector2 ( 1-(TextureOffset*11),1-(TextureOffset * 4)), // bottom right
                new Vector2 ( 1-(TextureOffset*11),1-(TextureOffset*3)),
                new Vector2 ( 1-(TextureOffset*12),1-(TextureOffset * 4)), // bottom left
		        new Vector2 ( 1-(TextureOffset*12),1-(TextureOffset*3)) // top left
            }, // top right  
            {
                new Vector2 ( 1-(TextureOffset*11),1-(TextureOffset * 4)), // bottom right
                new Vector2 ( 1-(TextureOffset*11),1-(TextureOffset*3)),
                new Vector2 ( 1-(TextureOffset*12),1-(TextureOffset * 4)), // bottom left
		        new Vector2 ( 1-(TextureOffset*12),1-(TextureOffset*3)) // top left
            }, // top right  
            {
                new Vector2 ( 1-(TextureOffset*11),1-(TextureOffset * 4)), // bottom right
                new Vector2 ( 1-(TextureOffset*11),1-(TextureOffset*3)),
                new Vector2 ( 1-(TextureOffset*12),1-(TextureOffset * 4)), // bottom left
		        new Vector2 ( 1-(TextureOffset*12),1-(TextureOffset*3)) // top left
            }, // top right  
            {
                new Vector2 ( 1-(TextureOffset*11),1-(TextureOffset * 4)), // bottom right
                new Vector2 ( 1-(TextureOffset*11),1-(TextureOffset*3)),
                new Vector2 ( 1-(TextureOffset*12),1-(TextureOffset * 4)), // bottom left
		        new Vector2 ( 1-(TextureOffset*12),1-(TextureOffset*3)) // top left
            }, // top right  
            {
                new Vector2 ( 1-(TextureOffset*11),1-(TextureOffset * 4)), // bottom right
                new Vector2 ( 1-(TextureOffset*11),1-(TextureOffset*3)),
                new Vector2 ( 1-(TextureOffset*12),1-(TextureOffset * 4)), // bottom left
		        new Vector2 ( 1-(TextureOffset*12),1-(TextureOffset*3)) // top left
            }, // top right  
            {
                new Vector2 ( 1-(TextureOffset*11),1-(TextureOffset * 4)), // bottom right
                new Vector2 ( 1-(TextureOffset*11),1-(TextureOffset*3)),
                new Vector2 ( 1-(TextureOffset*12),1-(TextureOffset * 4)), // bottom left
		        new Vector2 ( 1-(TextureOffset*12),1-(TextureOffset*3)) // top left
            }, // top right  
        }, // sea_lantern
        {
            {
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 15) ), // bottom right
		        new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 14) ), // top right  
                new Vector2 ( TextureOffset * 3 , 1 - (TextureOffset * 15) ), // bottom left
		        new Vector2 ( TextureOffset * 3 , 1 - (TextureOffset * 14) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 15) ), // bottom right
		        new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 14) ), // top right  
                new Vector2 ( TextureOffset * 3 , 1 - (TextureOffset * 15) ), // bottom left
		        new Vector2 ( TextureOffset * 3 , 1 - (TextureOffset * 14) ), // top left
            },
            { // log inside // +4 to left; -15 to bottom
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 15) ), // bottom right
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 14) ), // top right  
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 15) ), // bottom left
		        new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 14) ), // top left
            },
            { // log inside
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 15) ), // bottom right
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 14) ), // top right  
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 15) ), // bottom left
		        new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 14) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 15) ), // bottom right
		        new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 14) ), // top right  
                new Vector2 ( TextureOffset * 3 , 1 - (TextureOffset * 15) ), // bottom left
		        new Vector2 ( TextureOffset * 3 , 1 - (TextureOffset * 14) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 15) ), // bottom right
		        new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 14) ), // top right  
                new Vector2 ( TextureOffset * 3 , 1 - (TextureOffset * 15) ), // bottom left
		        new Vector2 ( TextureOffset * 3 , 1 - (TextureOffset * 14) ), // top left
            },
        }, // wood Y
        {
            { // log inside // +4 to left; -15 to bottom
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 15) ), // bottom right
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 14) ), // top right  
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 15) ), // bottom left
		        new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 14) ), // top left
            },
            { // log inside
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 15) ), // bottom right
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 14) ), // top right  
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 15) ), // bottom left
		        new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 14) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 15) ), // bottom right
		        new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 14) ), // top right  
                new Vector2 ( TextureOffset * 3 , 1 - (TextureOffset * 15) ), // bottom left
		        new Vector2 ( TextureOffset * 3 , 1 - (TextureOffset * 14) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 15) ), // bottom right
		        new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 14) ), // top right  
                new Vector2 ( TextureOffset * 3 , 1 - (TextureOffset * 15) ), // bottom left
		        new Vector2 ( TextureOffset * 3 , 1 - (TextureOffset * 14) ), // top left
            },
            { // BL-TL-TR-BR-BL
		        new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 14) ), // top right
		        new Vector2 ( TextureOffset * 3 , 1 - (TextureOffset * 14) ), // top left
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 15) ), // bottom right  
                new Vector2 ( TextureOffset * 3 , 1 - (TextureOffset * 15) ), // bottom left
            },
            {
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 14) ), // top right
		        new Vector2 ( TextureOffset * 3 , 1 - (TextureOffset * 14) ), // top left
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 15) ), // bottom right  
                new Vector2 ( TextureOffset * 3 , 1 - (TextureOffset * 15) ), // bottom left
            },
        }, // wood Z
        {

            { // BL-TL-TR-BR-BL
		        new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 14) ), // top right
		        new Vector2 ( TextureOffset * 3 , 1 - (TextureOffset * 14) ), // top left
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 15) ), // bottom right  
                new Vector2 ( TextureOffset * 3 , 1 - (TextureOffset * 15) ), // bottom left
            },
            {
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 14) ), // top right
		        new Vector2 ( TextureOffset * 3 , 1 - (TextureOffset * 14) ), // top left
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 15) ), // bottom right  
                new Vector2 ( TextureOffset * 3 , 1 - (TextureOffset * 15) ), // bottom left
            },
            {
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 14) ), // top right  
                new Vector2 ( TextureOffset * 3 , 1 - (TextureOffset * 14) ), // top left
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 15) ), // bottom right
                new Vector2 ( TextureOffset * 3 , 1 - (TextureOffset * 15) ), // bottom left
            },
            {
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 14) ), // top right  
                new Vector2 ( TextureOffset * 3 , 1 - (TextureOffset * 14) ), // top left
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 15) ), // bottom right
                new Vector2 ( TextureOffset * 3 , 1 - (TextureOffset * 15) ), // bottom left
            },
            { // log inside // +4 to left; -15 to bottom
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 15) ), // bottom right
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 14) ), // top right  
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 15) ), // bottom left
		        new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 14) ), // top left
            },
            { // log inside
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 15) ), // bottom right
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 14) ), // top right  
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 15) ), // bottom left
		        new Vector2 ( TextureOffset * 4 ,1 - (TextureOffset * 14) ), // top left
            },
        }, // wood X
        {

            { // BL-TL-TR-BR-BL
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 14) ), // bottom right
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 13) ), // top right  
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 14) ), // bottom left
		        new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 13) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 14) ), // bottom right
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 13) ), // top right  
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 14) ), // bottom left
		        new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 13) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 14) ), // bottom right
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 13) ), // top right  
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 14) ), // bottom left
		        new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 13) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 14) ), // bottom right
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 13) ), // top right  
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 14) ), // bottom left
		        new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 13) ), // top left
            },
            { // log inside // +4 to left; -15 to bottom
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 14) ), // bottom right
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 13) ), // top right  
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 14) ), // bottom left
		        new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 13) ), // top left
            },
            { // log inside
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 14) ), // bottom right
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 13) ), // top right  
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 14) ), // bottom left
		        new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 13) ), // top left
            },
        }, // leafs oak
        { { new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() }, }, // takes gap of tool in blockTypesArr
        { { new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() }, }, //
        { { new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() }, }, //
        { { new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() }, }, // 
        { { new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() },{ new Vector2(),new Vector2(),new Vector2(),new Vector2() }, }, //
        { // 4 to left of face, 5 to side, 6 to top; -9 to bottom
         //                 (horizontal, vertical)
            
            {
                new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 9) ), // bottom right
		        new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 8) ), // top right  
                new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 9) ), // bottom left
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 8) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 9) ), // bottom right
		        new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 8) ), // top right  
                new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 9) ), // bottom left
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 8) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 7 , 1 - (TextureOffset * 9) ), // bottom right
		        new Vector2 ( TextureOffset * 7 , 1 - (TextureOffset * 8) ), // top right  
                new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 9) ), // bottom left
		        new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 8) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 7 , 1 - (TextureOffset * 9) ), // bottom right
		        new Vector2 ( TextureOffset * 7 , 1 - (TextureOffset * 8) ), // top right  
                new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 9) ), // bottom left
		        new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 8) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 9) ), // bottom right
		        new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 8) ), // top right  
                new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 9) ), // bottom left
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 8) ), // top left
            },
            { // BL-TL-TR-BR-BL
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 9) ), // bottom right // face
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 8) ), // top right  
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 9) ), // bottom left
		        new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 8) ), // top left
            },
        }, // furnace // +x
        { // 4 to left of face, 5 to side, 6 to top; -9 to bottom
         //                 (horizontal, vertical)
            
            {
                new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 9) ), // bottom right
		        new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 8) ), // top right  
                new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 9) ), // bottom left
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 8) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 9) ), // bottom right
		        new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 8) ), // top right  
                new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 9) ), // bottom left
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 8) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 7 , 1 - (TextureOffset * 9) ), // bottom right
		        new Vector2 ( TextureOffset * 7 , 1 - (TextureOffset * 8) ), // top right  
                new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 9) ), // bottom left
		        new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 8) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 7 , 1 - (TextureOffset * 9) ), // bottom right
		        new Vector2 ( TextureOffset * 7 , 1 - (TextureOffset * 8) ), // top right  
                new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 9) ), // bottom left
		        new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 8) ), // top left
            },
            { // BL-TL-TR-BR-BL
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 9) ), // bottom right // face
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 8) ), // top right  
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 9) ), // bottom left
		        new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 8) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 9) ), // bottom right
		        new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 8) ), // top right  
                new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 9) ), // bottom left
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 8) ), // top left
            },
        }, // furnace // -x
        { // 4 to left of face, 5 to side, 6 to top; -9 to bottom
         //                 (horizontal, vertical)
            { // BL-TL-TR-BR-BL
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 9) ), // bottom right // face
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 8) ), // top right  
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 9) ), // bottom left
		        new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 8) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 9) ), // bottom right
		        new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 8) ), // top right  
                new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 9) ), // bottom left
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 8) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 7 , 1 - (TextureOffset * 9) ), // bottom right
		        new Vector2 ( TextureOffset * 7 , 1 - (TextureOffset * 8) ), // top right  
                new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 9) ), // bottom left
		        new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 8) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 7 , 1 - (TextureOffset * 9) ), // bottom right
		        new Vector2 ( TextureOffset * 7 , 1 - (TextureOffset * 8) ), // top right  
                new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 9) ), // bottom left
		        new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 8) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 9) ), // bottom right
		        new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 8) ), // top right  
                new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 9) ), // bottom left
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 8) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 9) ), // bottom right
		        new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 8) ), // top right  
                new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 9) ), // bottom left
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 8) ), // top left
            },
        }, // furnace // -z
        { // 4 to left of face, 5 to side, 6 to top; -9 to bottom
         //                 (horizontal, vertical)
            
            {
                new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 9) ), // bottom right
		        new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 8) ), // top right  
                new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 9) ), // bottom left
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 8) ), // top left
            },
            { // BL-TL-TR-BR-BL
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 9) ), // bottom right // face
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 8) ), // top right  
                new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 9) ), // bottom left
		        new Vector2 ( TextureOffset * 4 , 1 - (TextureOffset * 8) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 7 , 1 - (TextureOffset * 9) ), // bottom right
		        new Vector2 ( TextureOffset * 7 , 1 - (TextureOffset * 8) ), // top right  
                new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 9) ), // bottom left
		        new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 8) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 7 , 1 - (TextureOffset * 9) ), // bottom right
		        new Vector2 ( TextureOffset * 7 , 1 - (TextureOffset * 8) ), // top right  
                new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 9) ), // bottom left
		        new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 8) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 9) ), // bottom right
		        new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 8) ), // top right  
                new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 9) ), // bottom left
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 8) ), // top left
            },
            {
                new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 9) ), // bottom right
		        new Vector2 ( TextureOffset * 6 , 1 - (TextureOffset * 8) ), // top right  
                new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 9) ), // bottom left
		        new Vector2 ( TextureOffset * 5 , 1 - (TextureOffset * 8) ), // top left
            },
        }, // furnace // +z
        
    }; // ---------- end
}

public class Item
{
    public Player MainScript;

    public byte ArrLocation;

    public bool isTool;

    // block data
    public GameObject ParentChunk; // the parent chunk
    public Vector3 worldPosition; // the world position
    public BlockData.BlockTypes type; // the block type
    public bool interactable; // if interactable 
    public bool transparent; // if transparent (glass)
    public int StackSize; // the stacksize (defaults to 64)
    public int GuiIndex;

    public bool Scripted = false;

    // tool data
    public bool isArmor; // if its armor
    public BlockData.ArmorType armorType = BlockData.ArmorType.NONE; // armor type (NONE if not armor)
    public BlockData.ToolAbilityTypes toolAbilityType = BlockData.ToolAbilityTypes.NONE;
    public BlockData.ToolRank toolRank = BlockData.ToolRank.NONE;
    public BlockData.PartTypes partType = BlockData.PartTypes.NONE;

    public ToolRankData toolRankData = null;


    //furnace

    public float BurnTime = 1; // in seconds
    public float CookTime = 2; // in seconds

    public byte CookOutput = 0;

    public bool isBurnable = false;
    public bool isCookable = false;

    public Item(GameObject parent, Vector3 pos, BlockData.BlockTypes type = BlockData.BlockTypes.NONE, byte ArrLocation = 0, int stackSize = 64, BlockData.ArmorType armorType = BlockData.ArmorType.NONE, bool interactable = false, bool transparent = false, int guiIndex = -1, BlockData.ToolAbilityTypes toolAbilityType = BlockData.ToolAbilityTypes.NONE, BlockData.ToolRank toolRankVal = BlockData.ToolRank.NONE, BlockData.PartTypes toolType = BlockData.PartTypes.NONE)
    {
        MainScript = GameObject.FindGameObjectWithTag("MainScript").GetComponent<Player>();
        this.ParentChunk = parent;
        this.worldPosition = pos;
        this.type = type;
        this.interactable = interactable;
        this.transparent = transparent;
        this.StackSize = stackSize;
        this.ArrLocation = ArrLocation;
        GuiIndex = guiIndex;
        if (armorType != BlockData.ArmorType.NONE)
        {
            this.armorType = armorType;
            isArmor = true;
        }
        if (toolAbilityType != BlockData.ToolAbilityTypes.NONE)
        {
            this.toolAbilityType = toolAbilityType;
            this.isTool = true;
        }
        toolRank = toolRankVal;
        this.partType = toolType;
        /* ToolRankData temp = BlockData.ToolRankOffset[toolRankVal];
         toolRankData = new ToolRankData(temp.MineTime,temp.Durability,temp.Damage);*/
    }

    public virtual void SetLight(bool status, Vector3 pos) { }

    public virtual void SetGui(bool status) { }
    public virtual void OnPlace() { } // called upon placing
    public virtual void OnDestroy() { }
    public virtual void OnInteractEnter()
    {
        SetGui(true);
        MainScript.inventory.OpenGuiBlock = this;
    }
    public virtual void OnInteractExit() { } // not called yet
}

public class Empty : Item
{
    public Empty() : base(null, Vector3.zero, BlockData.BlockTypes.Air, ArrLocation: 0, transparent: true) { }
}

public class Block_Grass : Item // grass block
{
    public Block_Grass(GameObject parent, Vector3 pos) : base(parent, pos, BlockData.BlockTypes.Grass, 1) { } // default is  voxelUvs_grass
}
public class Block_Dirt : Item // drit block
{
    public Block_Dirt(GameObject parent, Vector3 pos) : base(parent, pos, BlockData.BlockTypes.Dirt, 2) { }
}
public class Block_Stone : Item // stone block
{
    public Block_Stone(GameObject parent, Vector3 pos) : base(parent, pos, BlockData.BlockTypes.Stone, 3) { }
}
public class Block_Bedrock : Item // bedrock block
{
    public Block_Bedrock(GameObject parent, Vector3 pos) : base(parent, pos, BlockData.BlockTypes.Bedrock, 4) { }
}

public class Block_Glass_none : Item // glass block
{
    public Block_Glass_none(GameObject parent, Vector3 pos) : base(parent, pos, BlockData.BlockTypes.Glass_none, 5, transparent: true) { }
}

public class Block_Cobblestone : Item // cobblestone block
{
    public Block_Cobblestone(GameObject parent, Vector3 pos) : base(parent, pos, BlockData.BlockTypes.Cobblestone, 6)
    {
        isCookable = true;
        CookOutput = 3;
    }
}

public class Block_Brick_Wall : Item // brick wall block
{
    public Block_Brick_Wall(GameObject parent, Vector3 pos) : base(parent, pos, BlockData.BlockTypes.BrickWall, 7) { }
}

public class Block_Crafting_Table : Item // crafting table
{
    public Block_Crafting_Table(GameObject parent, Vector3 pos) : base(parent, pos, BlockData.BlockTypes.CraftingTable, 8, guiIndex: 0, interactable: true)
    {
        isBurnable = true;
        BurnTime = 15;
        isCookable = true;
        CookOutput = 1; // need to be charcoal
        Scripted = true;
    }

    public override void SetGui(bool status) // open craftin table gui
    {
        MainScript.inventory.SetGui(GuiIndex, status);
    }

    public override void OnInteractEnter()
    {
        base.OnInteractEnter();
    }

}

public class Block_Planks_Oak : Item // plank oak
{
    public Block_Planks_Oak(GameObject parent, Vector3 pos) : base(parent, pos, BlockData.BlockTypes.Planks_Oak, 9)
    {
        isBurnable = true;
        BurnTime = 15;
        isCookable = true;
        CookOutput = 1; // need to ba charcoal
    }
}

public class Block_Snowy_Grass : Item // snowy grass
{
    public Block_Snowy_Grass(GameObject parent, Vector3 pos) : base(parent, pos, BlockData.BlockTypes.Snowy_Grass, 10) { }
}

public class Block_Sea_Lantern : Item
{
    public Block_Sea_Lantern(GameObject parent, Vector3 pos) : base(parent, pos, BlockData.BlockTypes.Sea_Lantern, 11, transparent: true)
    {
        Scripted = true;
    }

    public override void SetLight(bool status, Vector3 position)
    {
        if (status) // if turnin lamp on
        {
            GameObject light = new GameObject("Sea_Lentern_Light"); // create new lamp
            Light light_comp = light.AddComponent<Light>();

            light_comp.color = Color.white; // set values
            light_comp.intensity = 1.5f;
            light_comp.range = 10;
            light.transform.SetParent(GameObject.Find("LightSources").transform); // set parent
            light.transform.position = position + (Vector3.one / 2); // move to requested position
            //base.player.printF("set light " + status.ToString() + ", " + position + ", " + light.transform.position); 
        }
        else // if turning lamp of
        {
            GameObject parent = GameObject.Find("LightSources"); // get the lightSources parent
            int count = parent.transform.childCount; // get the child count
            GameObject child; // child object
            for (int i = 0; i < count; i++) // run on child count
            {
                child = parent.transform.GetChild(i).gameObject; // get the game object of child[i]
                if (Util.PositionInBlock(child.transform.position, position)) // if in position of requested block
                {
                    child.SetActive(false); // turn of
                }
            }
        }
    }

}

public class Block_Log_Oak : Item // log oak
{ // 12 for Y, 13 for X, 14 for Z
    public Block_Log_Oak(GameObject parent, Vector3 pos) : base(parent, pos, BlockData.BlockTypes.Log_Oak, 12)
    {
        isBurnable = true;
        BurnTime = 15;
        isCookable = true;
        CookOutput = 1; // need to ba charcoal
    }
}
public class Block_Leafs_Def : Item // log oak
{ // 12 for Y, 13 for X, 14 for Z
    public Block_Leafs_Def(GameObject parent, Vector3 pos) : base(parent, pos, BlockData.BlockTypes.Leafes_Def, 15, transparent: true) { }
}

public class Block_Furnace : Item
{
    public FurnaceUpdater furnaceUpdater;
    bool guiOpen;
    public Block_Furnace(GameObject parent, Vector3 pos) : base(parent, pos, BlockData.BlockTypes.Furnace, 22, guiIndex: 1)
    {
        Scripted = true;
        interactable = true;
    }

    public override void SetGui(bool status)
    {
        MainScript.inventory.SetGui(GuiIndex, status); // start gui
        guiOpen = status;
    }

    public void CalculateSmelting() // runs like update
    {
        if ((furnaceUpdater.Feul.Count > 0 || furnaceUpdater.HasFeul) && furnaceUpdater.Input.Count > 0 && (furnaceUpdater.Output.Count == 0 || (furnaceUpdater.Output.Count > 0 && furnaceUpdater.Output.Peek().ArrLocation == furnaceUpdater.Input.Peek().CookOutput))) // if have feul, have input and output is either empty or of the same type as the output from input
        {
            if (!furnaceUpdater.HasFeul && furnaceUpdater.Feul.Count > 0 && furnaceUpdater.Input.Count > 0) // if no feul, get feul out, and updateGraphics
            {
                furnaceUpdater.HasFeul = true; // update has fuel
                furnaceUpdater.burnTime = furnaceUpdater.Feul.Peek().BurnTime; // get new burntime
                furnaceUpdater.topBurnTime = furnaceUpdater.burnTime;
                furnaceUpdater.Feul.Pop(); // remove feul
                RefreshGraphicSlots(); // refresh the slots
                MainScript.inventory.UpdateGraphics(); // update graphics
            }
            if (furnaceUpdater.burnTime <= 0) // if burned all, update no feul 
            {
                furnaceUpdater.HasFeul = false;
                furnaceUpdater.Cooking = false;
            }
            if (!furnaceUpdater.Cooking && furnaceUpdater.Input.Count > 0 && furnaceUpdater.HasFeul) // if not cooking and has input and has feul
            {
                furnaceUpdater.Cooking = true; // start cooking
                furnaceUpdater.cookTime = 0; // reset timer
                furnaceUpdater.topCookTime = furnaceUpdater.Input.Peek().CookTime; // reset cooking timer
            }
            if (furnaceUpdater.Cooking && furnaceUpdater.cookTime < furnaceUpdater.topCookTime && furnaceUpdater.HasFeul) // if coocking, and didnt finish to cook
            {
                furnaceUpdater.cookTime += Time.deltaTime; // add to timer
            }
            if (furnaceUpdater.Cooking && furnaceUpdater.cookTime >= furnaceUpdater.topCookTime) // if coocking and timer got to time // bug here... output goes to all furnaces in chunk
            {
                Item temp = furnaceUpdater.Input.Pop(); // remove from input
                //Debug.Log(furnaceUpdater.pos);
                furnaceUpdater.Output.Push(Util.GetNewItem(temp.CookOutput)); // add new instance of the output
                furnaceUpdater.Cooking = false; // not cooking 
                furnaceUpdater.cookTime = 0; // reset timer
                RefreshGraphicSlots();
                MainScript.inventory.UpdateGraphics();
            }
            if (furnaceUpdater.Input.Count == 0) // if out of input
            {
                furnaceUpdater.Cooking = false; // stop cooking
            }
        }
        else // if cant cook
        {
            if (furnaceUpdater.cookTime > 0) // if cook timer is on
            {
                furnaceUpdater.cookTime -= Time.deltaTime; // reduce
            }
        }
        if (furnaceUpdater.burnTime > 0) // if burnTimer not done
        {
            furnaceUpdater.burnTime -= Time.deltaTime; // reduce
        }
        if (guiOpen)
        {
            MainScript.inventory.FurnaceProggressSliders[0].value = furnaceUpdater.cookTime / furnaceUpdater.topCookTime; // update sliders
            MainScript.inventory.FurnaceProggressSliders[1].value = furnaceUpdater.burnTime / furnaceUpdater.topBurnTime;
            Debug.Log(furnaceUpdater.pos + ", cook:" + MainScript.inventory.FurnaceProggressSliders[0].value + ", burn:" + MainScript.inventory.FurnaceProggressSliders[1].value);
        }
    }

    public override void OnPlace()
    {
        furnaceUpdater = ParentChunk.AddComponent<FurnaceUpdater>();
        furnaceUpdater.furnace = this;
        furnaceUpdater.UpdateFurnace = true;
    }
    public override void OnDestroy()
    {
        Object.Destroy(furnaceUpdater);
        furnaceUpdater.UpdateFurnace = false;
    }

    public override void OnInteractEnter()
    {
        Debug.Log(worldPosition);
        MainScript.inventory.OpenFurnace = furnaceUpdater;
        MainScript.inventory.FurnaceSpace[0] = (furnaceUpdater.Feul.Count > 0) ? furnaceUpdater.Feul.Peek() : null;
        MainScript.inventory.FurnaceSpace[1] = (furnaceUpdater.Input.Count > 0) ? furnaceUpdater.Input.Peek() : null;
        MainScript.inventory.FurnaceSpace[2] = (furnaceUpdater.Output.Count > 0) ? furnaceUpdater.Output.Peek() : null;

        MainScript.inventory.FurnaceSpaceCounter[0] = furnaceUpdater.Feul.Count;
        MainScript.inventory.FurnaceSpaceCounter[1] = furnaceUpdater.Input.Count;
        MainScript.inventory.FurnaceSpaceCounter[2] = furnaceUpdater.Output.Count;
        MainScript.inventory.SetFurnaceStatus();
        base.OnInteractEnter();
    }

    public void RefreshGraphicSlots()
    {
        MainScript.inventory.FurnaceSpace[0] = (furnaceUpdater.Feul.Count > 0) ? furnaceUpdater.Feul.Peek() : null;
        MainScript.inventory.FurnaceSpace[1] = (furnaceUpdater.Input.Count > 0) ? furnaceUpdater.Input.Peek() : null;
        MainScript.inventory.FurnaceSpace[2] = (furnaceUpdater.Output.Count > 0) ? furnaceUpdater.Output.Peek() : null;

        MainScript.inventory.FurnaceSpaceCounter[0] = furnaceUpdater.Feul.Count;
        MainScript.inventory.FurnaceSpaceCounter[1] = furnaceUpdater.Input.Count;
        MainScript.inventory.FurnaceSpaceCounter[2] = furnaceUpdater.Output.Count;
        MainScript.inventory.SetFurnaceStatus();
    }
} // non functional currently
public class FurnaceUpdater : MonoBehaviour
{
    public Block_Furnace furnace;
    public Vector3 pos;
    public bool UpdateFurnace = false;

    public float burnTime = 0;
    public float topBurnTime = 0;
    public float cookTime = 0;
    public float topCookTime = 0;
    public bool HasFeul = false;

    public bool Cooking = false;

    public Stack<Item> Feul = new Stack<Item>();
    public Stack<Item> Input = new Stack<Item>();
    public Stack<Item> Output = new Stack<Item>();

    void Start()
    {
        Feul = new Stack<Item>();
        Input = new Stack<Item>();
        Output = new Stack<Item>();
        pos = furnace.worldPosition;
    }

    void Update()
    {
        if (UpdateFurnace)
            furnace.CalculateSmelting();
    }
}

// tools: -------------------------------------------------------------------------------------------------------------------------------------------------------------

public class Tool_Pickaxe_Iron : Item
{
    public Tool_Pickaxe_Iron() : base(null, Vector3.zero, ArrLocation: 16, stackSize: 1, toolAbilityType: BlockData.ToolAbilityTypes.Both, toolRankVal: BlockData.ToolRank.Iron, toolType: BlockData.PartTypes.Iron_Pickaxe, type: BlockData.BlockTypes.Tool)
    {
        toolRankData = new ToolRankData(BlockData.ToolRankOffset[toolRank].MineTime, BlockData.ToolRankOffset[toolRank].Durability, BlockData.ToolRankOffset[toolRank].Damage);
    }
}

public class Tool_Axe_Iron : Item
{
    public Tool_Axe_Iron() : base(null, Vector3.zero, ArrLocation: 17, stackSize: 1, toolAbilityType: BlockData.ToolAbilityTypes.Both, toolRankVal: BlockData.ToolRank.Iron, toolType: BlockData.PartTypes.Iron_Axe, type: BlockData.BlockTypes.Tool)
    {
        toolRankData = new ToolRankData(BlockData.ToolRankOffset[toolRank].MineTime, BlockData.ToolRankOffset[toolRank].Durability, BlockData.ToolRankOffset[toolRank].Damage);
    }
}

public class Tool_Pickaxe_Stone : Item
{
    public Tool_Pickaxe_Stone() : base(null, Vector3.zero, ArrLocation: 19, stackSize: 1, toolAbilityType: BlockData.ToolAbilityTypes.Both, toolRankVal: BlockData.ToolRank.Stone, toolType: BlockData.PartTypes.Stone_Pickaxe, type: BlockData.BlockTypes.Tool)
    {
        toolRankData = new ToolRankData(BlockData.ToolRankOffset[toolRank].MineTime, BlockData.ToolRankOffset[toolRank].Durability, BlockData.ToolRankOffset[toolRank].Damage);
    }
}

public class Tool_Axe_Stone : Item
{
    public Tool_Axe_Stone() : base(null, Vector3.zero, ArrLocation: 20, stackSize: 1, toolAbilityType: BlockData.ToolAbilityTypes.Both, toolRankVal: BlockData.ToolRank.Stone, toolType: BlockData.PartTypes.Stone_Axe, type: BlockData.BlockTypes.Tool)
    {
        toolRankData = new ToolRankData(BlockData.ToolRankOffset[toolRank].MineTime, BlockData.ToolRankOffset[toolRank].Durability, BlockData.ToolRankOffset[toolRank].Damage);
    }
}


// parts

public class Part_Stick : Item
{
    public Part_Stick() : base(null, Vector3.zero, ArrLocation: 18, toolType: BlockData.PartTypes.Stick)
    {
        isBurnable = true;
        BurnTime = 5;
    }
}