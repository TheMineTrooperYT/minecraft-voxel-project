﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public static class Util
{
    public static bool Between(float toCheck, float high, float low)
    {
        return !(toCheck >= high || toCheck <= low);
    }
    public static bool BetweenInc(float toCheck, float high, float low)
    {
        return !(toCheck > high || toCheck < low);
    }

    public static bool PositionInBlock(Vector3 positionToCheck,Vector3 blockZeroPosition)
    {
        return (BetweenInc(positionToCheck.x, blockZeroPosition.x + 1, blockZeroPosition.x) && BetweenInc(positionToCheck.y, blockZeroPosition.y + 1, blockZeroPosition.y) && BetweenInc(positionToCheck.z, blockZeroPosition.z + 1, blockZeroPosition.z));
    }

    public static Item GetNewBlock(BlockData.BlockTypes type) // returns a new instance of the block type
    {
        Item retBlock = null;

        switch (type)
        {
            case BlockData.BlockTypes.Grass:
                retBlock = new Block_Grass(null, Vector3.zero); // grass
                break;
            case BlockData.BlockTypes.Dirt:
                retBlock = new Block_Dirt(null, Vector3.zero); // dirt
                break;
            case BlockData.BlockTypes.Stone:
                retBlock = new Block_Stone(null, Vector3.zero); // stone
                break;
            case BlockData.BlockTypes.Bedrock:
                retBlock = new Block_Bedrock(null, Vector3.zero); // bedrock
                break;
            case BlockData.BlockTypes.Cobblestone:
                retBlock = new Block_Cobblestone(null, Vector3.zero); // bedrock
                break;
            case BlockData.BlockTypes.Glass_none:
                retBlock = new Block_Glass_none(null, Vector3.zero); // normal glass
                break;
            case BlockData.BlockTypes.BrickWall:
                retBlock = new Block_Brick_Wall(null, Vector3.zero); // brick_wall
                break;
            case BlockData.BlockTypes.CraftingTable:
                retBlock = new Block_Crafting_Table(null, Vector3.zero); // crafting table
                break;
            case BlockData.BlockTypes.Planks_Oak:
                retBlock = new Block_Planks_Oak(null, Vector3.zero); // crafting table
                break;
            case BlockData.BlockTypes.Snowy_Grass:
                retBlock = new Block_Snowy_Grass(null, Vector3.zero); // snowy grass
                break;
            case BlockData.BlockTypes.Sea_Lantern:
                retBlock = new Block_Sea_Lantern(null, Vector3.zero);
                break;
            case BlockData.BlockTypes.Log_Oak:
                retBlock = new Block_Log_Oak(null, Vector3.zero);
                break;
            case BlockData.BlockTypes.Furnace:
                retBlock = new Block_Furnace(null,Vector3.zero);
                break;
            default:
                break;
        }
        return retBlock;
    }

    public static Item GetNewItem(byte ArrLocation) // return a new instance of the item in BlockData.ItemTypesArr[ArrLocation]
    {
        Item refer = BlockData.ItemTypesArr[ArrLocation];
        Item ret = null;
        if (refer.type != BlockData.BlockTypes.Tool && refer.type != BlockData.BlockTypes.NONE) // blocks
        {
            ret = GetNewBlock(refer.type);
        }
        else // tools and parts
        {
            switch (refer.partType)
            {
                case BlockData.PartTypes.Wood_Shovel: // not existent yet
                    break;
                case BlockData.PartTypes.Wood_Hoe: // not existent yet
                    break;
                case BlockData.PartTypes.Wood_Pickaxe: // not existent yet
                    break;
                case BlockData.PartTypes.Wood_Axe: // not existent yet
                    break;
                case BlockData.PartTypes.Wood_Sword: // not existent yet
                    break;
                case BlockData.PartTypes.Stone_Shovel: // not existent yet
                    break;
                case BlockData.PartTypes.Stone_Hoe: // not existent yet
                    break;
                case BlockData.PartTypes.Stone_Pickaxe:
                    ret = new Tool_Pickaxe_Stone();
                    break;
                case BlockData.PartTypes.Stone_Axe:
                    ret = new Tool_Axe_Stone();
                    break;
                case BlockData.PartTypes.Stone_Sword: // not existent yet
                    break;
                case BlockData.PartTypes.Iron_Shovel: // not existent yet
                    break;
                case BlockData.PartTypes.Iron_Hoe: // not existent yet
                    break;
                case BlockData.PartTypes.Iron_Pickaxe:
                    ret = new Tool_Pickaxe_Iron();
                    break;
                case BlockData.PartTypes.Iron_Axe:
                    ret = new Tool_Axe_Iron();
                    break;
                case BlockData.PartTypes.Iron_Sword: // not existent yet
                    break;
                case BlockData.PartTypes.Gold_Shovel: // not existent yet
                    break;
                case BlockData.PartTypes.Gold_Hoe: // not existent yet
                    break;
                case BlockData.PartTypes.Gold_Pickaxe: // not existent yet
                    break;
                case BlockData.PartTypes.Gold_Axe: // not existent yet
                    break;
                case BlockData.PartTypes.Gold_Sword: // not existent yet
                    break;
                case BlockData.PartTypes.Diamond_Shovel: // not existent yet
                    break;
                case BlockData.PartTypes.Diamond_Hoe: // not existent yet
                    break;
                case BlockData.PartTypes.Diamond_Pickaxe: // not existent yet
                    break;
                case BlockData.PartTypes.Diamond_Axe: // not existent yet
                    break;
                case BlockData.PartTypes.Diamond_Sword: // not existent yet
                    break;
                case BlockData.PartTypes.Stick:
                    ret = new Part_Stick();
                    break;
                default:
                    break;
            }
        }
        return ret;
    }
}
/*
class Utils : MonoBehaviour
{
    public Text ErroMsg;

    void Start()
    {

    }

    void update()
    {

    }
}*/
