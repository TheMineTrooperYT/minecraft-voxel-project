﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InventoryButton : MonoBehaviour, IPointerClickHandler
{

    public Player MainScript;

    public int id;
    public bool inBackpack;
    public bool inCraftingTable;
    public bool inFurnace;
    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
            MainScript.inventory.RightClickedInvButton(id, inBackpack, inCraftingTable, inFurnace);
    }

    public void Clicked()
    {
        print(id + ", " + inBackpack + ", " + inCraftingTable + ", " + inFurnace);
        MainScript.inventory.ClickedInvButton(id, inBackpack,inCraftingTable,inFurnace);
    }
}
