﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DurabilityHandeler : MonoBehaviour {

    public GameObject SliderFillArea;
    public Slider slider;

    void Start()
    {
        SliderFillArea = gameObject.transform.GetChild(1).gameObject;
        slider = GetComponent<Slider>();
    }

    void Update()
    {
        if (slider.value == 0 && SliderFillArea.activeSelf)
        {
            SliderFillArea.SetActive(false);
        }
        if (slider.value > 0 && !SliderFillArea.activeSelf)
        {
            SliderFillArea.SetActive(true);
        }
    }
}
